// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.refcodes.component.InitializeException;
import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordImpl;
import org.refcodes.tabular.Records;

/**
 * Unit test for the {@link TrimLoggerComposite} {@link TrimLogger}
 * implementation.
 */
public class StatisticalCompositeTrimLoggerTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOGGERS = 25;
	private static final int RUNS = 100000;
	private TrimLoggerComposite<Object> _compositeTrimLogger;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final TestTrimLogger[] _trimLoggers = new TestTrimLogger[LOGGERS];

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() {
		for ( int i = 0; i < _trimLoggers.length; i++ ) {
			_trimLoggers[i] = new TestTrimLogger( i );
		}
		_compositeTrimLogger = new TrimLoggerComposite<>( _trimLoggers );
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testLog() throws InitializeException, IOException {
		_compositeTrimLogger.initialize();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, _trimLoggers );
		for ( int i = 0; i < RUNS; i++ ) {
			// System.out.print( i + " " );
			final Record<?> theRecord = new RecordImpl<Object>();
			_compositeTrimLogger.log( theRecord );
		}
		_compositeTrimLogger.flush();
		_compositeTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( true, false, false, false, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerInvocation( RUNS, 0, 0, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, _trimLoggers );
		_compositeTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, _trimLoggers );
	}

	@Test
	public void testFindLogs() throws InitializeException, IOException {
		_compositeTrimLogger.initialize();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, _trimLoggers );
		Records<?> eRecords;
		final int theRuns = RUNS / 50;
		for ( int i = 0; i < theRuns; i++ ) {
			eRecords = _compositeTrimLogger.findLogs();
			assertEquals( LOGGERS, TestTrimLoggerUtility.getSize( eRecords ) );
		}
		_compositeTrimLogger.flush();
		_compositeTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( false, true, false, false, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerInvocation( 0, theRuns * LOGGERS, 0, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, _trimLoggers );
		_compositeTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, _trimLoggers );
	}

	@Test
	public void testDeleteLogs() throws InitializeException, IOException, BadCriteriaException {
		_compositeTrimLogger.initialize();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, _trimLoggers );
		final int theRuns = RUNS / 50;
		for ( int i = 0; i < theRuns; i++ ) {
			_compositeTrimLogger.deleteLogs( null );
		}
		_compositeTrimLogger.flush();
		_compositeTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, true, false, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerInvocation( 0, 0, theRuns * LOGGERS, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, _trimLoggers );
		_compositeTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, _trimLoggers );
	}

	@Test
	public void testClear() throws InitializeException, IOException {
		_compositeTrimLogger.initialize();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, _trimLoggers );
		final int theRuns = 1;
		for ( int i = 0; i < theRuns; i++ ) {
			_compositeTrimLogger.clear();
		}
		_compositeTrimLogger.flush();
		_compositeTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, true, _trimLoggers );
		TestTrimLoggerUtility.doAssertLoggerInvocation( 0, 0, 0, theRuns * LOGGERS, _trimLoggers );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, _trimLoggers );
		_compositeTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, _trimLoggers );
	}
}
