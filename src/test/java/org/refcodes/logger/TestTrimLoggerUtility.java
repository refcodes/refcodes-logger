// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.refcodes.component.ComponentComposite;

/**
 * Utility class helpful for test cases regarding the {@link TrimLogger} and in
 * conjunction with other {@link Logger} instances.
 */
public final class TestTrimLoggerUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private TestTrimLoggerUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Asserts the state change invocation counts regarding the
	 * {@link ComponentComposite}'s supported state change methods.
	 *
	 * @param aInitializeCount The according invocation count.
	 * @param aStartCount The according invocation count.
	 * @param aPauseCount The according invocation count.
	 * @param aResumeCount The according invocation count.
	 * @param aStopCount The according invocation count.
	 * @param aDestroyCount The according invocation count.
	 * @param aDecomposeCount The according invocation count.
	 * @param aFlushCount The according invocation count.
	 * @param aTrimLoggers the trim loggers
	 */
	public static void doAssertCompositeComponentInvocation( int aInitializeCount, int aStartCount, int aPauseCount, int aResumeCount, int aStopCount, int aDestroyCount, int aDecomposeCount, int aFlushCount, TestTrimLogger[] aTrimLoggers ) {
		for ( TestTrimLogger aTrimLogger : aTrimLoggers ) {
			assertEquals( aInitializeCount, aTrimLogger.getInitializeCount() );
			assertEquals( aStartCount, aTrimLogger.getStartCount() );
			assertEquals( aPauseCount, aTrimLogger.getPauseCount() );
			assertEquals( aResumeCount, aTrimLogger.getResumeCount() );
			assertEquals( aStopCount, aTrimLogger.getStopCount() );
			assertEquals( aDestroyCount, aTrimLogger.getDestroyCount() );
			assertEquals( aDecomposeCount, aTrimLogger.getDecomposeCount() );
			assertEquals( aFlushCount, aTrimLogger.getFlushCount() );
		}
	}

	/**
	 * Asserts the method invocation counts regarding the {@link TrimLogger}
	 * 'supported log related methods.
	 *
	 * @param aOverallLogCount The according invocation count.
	 * @param aOverallFindLogsCount The according invocation count.
	 * @param aOverallDeleteLogsCount The according invocation count.
	 * @param aOverallClearCount The according invocation count.
	 * @param aTrimLoggers the trim loggers
	 */
	public static void doAssertLoggerInvocation( int aOverallLogCount, int aOverallFindLogsCount, int aOverallDeleteLogsCount, int aOverallClearCount, TestTrimLogger[] aTrimLoggers ) {
		int theOverallLogCount = 0;
		int theOverallFindLogsCount = 0;
		int theOverallDeleteLogsCount = 0;
		int theOverallClearCount = 0;
		for ( TestTrimLogger aTrimLogger : aTrimLoggers ) {
			theOverallLogCount += aTrimLogger.getLogCount();
			theOverallFindLogsCount += aTrimLogger.getFindLogsCount();
			theOverallDeleteLogsCount += aTrimLogger.getDeleteLogsCount();
			theOverallClearCount += aTrimLogger.getClearCount();
		}
		assertEquals( aOverallLogCount, theOverallLogCount );
		assertEquals( aOverallFindLogsCount, theOverallFindLogsCount );
		assertEquals( aOverallDeleteLogsCount, theOverallDeleteLogsCount );
		assertEquals( aOverallClearCount, theOverallClearCount );
	}

	/**
	 * Asserts the method invocation on all loggers regarding the
	 * {@link TrimLogger} 'supported log related methods.
	 *
	 * @param hasLog True in case the according method is expected to have been
	 *        invoked on each logger.
	 * @param hasFindLogs True in case the according method is expected to have
	 *        been invoked on each logger.
	 * @param hasDeleteLogs True in case the according method is expected to
	 *        have been invoked on each logger.
	 * @param hasClear True in case the according method is expected to have
	 *        been invoked on each logger.
	 * @param aTrimLoggers the trim loggers
	 */
	public static void doAssertLoggerTouch( boolean hasLog, boolean hasFindLogs, boolean hasDeleteLogs, boolean hasClear, TestTrimLogger[] aTrimLoggers ) {
		for ( TestTrimLogger aTrimLogger : aTrimLoggers ) {
			assertTrue( aTrimLogger.getClearCount() != 0 == hasClear );
			assertTrue( aTrimLogger.getDeleteLogsCount() != 0 == hasDeleteLogs );
			assertTrue( aTrimLogger.getFindLogsCount() != 0 == hasFindLogs );
			assertTrue( aTrimLogger.getLogCount() != 0 == hasLog );
		}
	}

	/**
	 * Iterates through the {@link Iterator} and counts the elements, returns
	 * the element count.
	 *
	 * @param e the e
	 * 
	 * @return The number of elements originally contained in the
	 *         {@link Iterator}.
	 */
	public static int getSize( Iterator<?> e ) {
		int theSize = 0;
		while ( e.hasNext() ) {
			theSize++;
			e.next();
		}
		return theSize;
	}
}
