// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.criteria.CriteriaSugar.*;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.refcodes.component.InitializeException;
import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.tabular.FieldImpl;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordImpl;
import org.refcodes.tabular.Records;
import org.refcodes.tabular.StringColumn;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;

/**
 * Unit test for the {@link PartedTrimLogger} {@link TrimLogger} implementation.
 */
public class StatisticalPartedTrimLoggerTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String KEY_PARTITION_UID = "partition_uid";
	private static final int LOGGERS = 25;
	private static final int RUNS = 100000;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TestPartedTrimLogger _partedTrimLogger;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() {
		_partedTrimLogger = new TestPartedTrimLogger( new StringColumn( KEY_PARTITION_UID ), new TestTrimLoggerFactory(), true );
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testLog() throws InitializeException, IOException {
		_partedTrimLogger.initialize();
		// ---------------------------------------------------------------------
		// As we do not have any logger yet (first one is created upon first
		// partition detected when logging).
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 0, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, getLoggers() );
		int theCurrentLogger = 0;
		for ( int i = 0; i < RUNS; i++ ) {
			final Record<?> theRecord = new RecordImpl<Object>( new FieldImpl<String>( KEY_PARTITION_UID, toPartitionName( theCurrentLogger ) ) );
			theCurrentLogger++;
			if ( theCurrentLogger == LOGGERS ) {
				theCurrentLogger = 0;
			}
			_partedTrimLogger.log( theRecord );
		}
		// ---------------------------------------------------------------------
		// As we logged to all partitions, all loggers should have been
		// initialized (as of auto initialize set to true during setup.
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		_partedTrimLogger.flush();
		_partedTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( true, false, false, false, getLoggers() );
		TestTrimLoggerUtility.doAssertLoggerInvocation( RUNS, 0, 0, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, getLoggers() );
		_partedTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, getLoggers() );
	}

	@Test
	public void testFindLogs() throws InitializeException, IOException {
		_partedTrimLogger.initialize();
		// ---------------------------------------------------------------------
		// As we do not have any logger yet (first one is created upon first
		// partition detected when logging).
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 0, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, getLoggers() );
		// ---------------------------------------------------------------------
		// Initialize the partitions:
		// ---------------------------------------------------------------------
		initPartitions();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		Records<?> eRecords;
		int theCurrentLogger = 0;
		for ( int i = 0; i < RUNS; i++ ) {
			final Criteria theCriteria = and( equalWith( KEY_PARTITION_UID, toPartitionName( theCurrentLogger ) ), not( greaterThan( "Esel", 5 ) ) );
			theCurrentLogger++;
			if ( theCurrentLogger == LOGGERS ) {
				theCurrentLogger = 0;
			}
			eRecords = _partedTrimLogger.findLogs( theCriteria );
			assertEquals( 1, TestTrimLoggerUtility.getSize( eRecords ) );
		}
		_partedTrimLogger.flush();
		_partedTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( true, true, false, false, getLoggers() );
		// ---------------------------------------------------------------------
		// As of initializing the atomic TrimLogger instances by logging to the
		// actual partition, each TrimLogger has a log count of 1:
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertLoggerInvocation( LOGGERS, RUNS, 0, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, getLoggers() );
		_partedTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, getLoggers() );
	}

	@Test
	public void testDeleteLogs() throws InitializeException, IOException, BadCriteriaException {
		_partedTrimLogger.initialize();
		// ---------------------------------------------------------------------
		// As we do not have any logger yet (first one is created upon first
		// partition detected when logging).
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 0, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, getLoggers() );
		// ---------------------------------------------------------------------
		// Initialize the partitions:
		// ---------------------------------------------------------------------
		initPartitions();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		int theCurrentLogger = 0;
		for ( int i = 0; i < RUNS; i++ ) {
			final Criteria theCriteria = and( equalWith( KEY_PARTITION_UID, toPartitionName( theCurrentLogger ) ), not( greaterThan( "Esel", 5 ) ) );
			theCurrentLogger++;
			if ( theCurrentLogger == LOGGERS ) {
				theCurrentLogger = 0;
			}
			_partedTrimLogger.deleteLogs( theCriteria );
		}
		_partedTrimLogger.flush();
		_partedTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( true, false, true, false, getLoggers() );
		// ---------------------------------------------------------------------
		// As of initializing the atomic TrimLogger instances by logging to the
		// actual partition, each TrimLogger has a log count of 1:
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertLoggerInvocation( LOGGERS, 0, RUNS, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, getLoggers() );
		_partedTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, getLoggers() );
	}

	@Test
	public void testClear() throws InitializeException, IOException {
		_partedTrimLogger.initialize();
		// ---------------------------------------------------------------------
		// As we do not have any logger yet (first one is created upon first
		// partition detected when logging).
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 0, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		TestTrimLoggerUtility.doAssertLoggerTouch( false, false, false, false, getLoggers() );
		// ---------------------------------------------------------------------
		// Initialize the partitions:
		// ---------------------------------------------------------------------
		initPartitions();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 0, 0, 0, getLoggers() );
		final int theRuns = 1;
		for ( int i = 0; i < theRuns; i++ ) {
			_partedTrimLogger.clear();
		}
		_partedTrimLogger.flush();
		_partedTrimLogger.destroy();
		TestTrimLoggerUtility.doAssertLoggerTouch( true, false, false, true, getLoggers() );
		// ---------------------------------------------------------------------
		// As of initializing the atomic TrimLogger instances by logging to the
		// actual partition, each TrimLogger has a log count of 1:
		// ---------------------------------------------------------------------
		TestTrimLoggerUtility.doAssertLoggerInvocation( LOGGERS, 0, 0, theRuns * LOGGERS, getLoggers() );
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 0, 1, getLoggers() );
		_partedTrimLogger.decompose();
		TestTrimLoggerUtility.doAssertCompositeComponentInvocation( 1, 0, 0, 0, 0, 1, 1, 1, getLoggers() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void initPartitions() {
		for ( int i = 0; i < LOGGERS; i++ ) {
			final Record<?> theRecord = new RecordImpl<Object>( new FieldImpl<String>( KEY_PARTITION_UID, toPartitionName( i ) ) );
			_partedTrimLogger.log( theRecord );
		}
	}

	private String toPartitionName( int aPartitionNumber ) {
		return "partition-" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( Integer.toString( aPartitionNumber ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString();
		// return "partition-" + AlignTextUtility.toAlignRight( "" +
		// aPartitionNumber, 5, '0' );
	}

	private TestTrimLogger[] getLoggers() {
		return _partedTrimLogger.getLoggers().toArray( new TestTrimLogger[_partedTrimLogger.getLoggers().size()] );
	}
}
