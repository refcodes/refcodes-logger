// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.io.IOException;

import org.refcodes.component.ComponentComposite;
import org.refcodes.component.InitializeException;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.criteria.Criteria;
import org.refcodes.runtime.Execution;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordImpl;
import org.refcodes.tabular.Records;
import org.refcodes.tabular.RecordsImpl;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;

/**
 * The {@link TestTrimLogger} implements the {@link TrimLogger} interface and is
 * used for testing the various logger functionalities regarding partitioning
 * and clustering (composite). The actual log content is not regarded by this
 * logger, moreover calls to its methods are counted to be able to determine
 * whether the various instances are called as expected when being encapsulated
 * for example in a {@link PartedTrimLogger} or a {@link TrimLoggerComposite}.
 */
public class TestTrimLogger implements TrimLogger<Object>, ComponentComposite {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _loggerName;

	private int _findLogsCount = 0;

	private int _clearCount = 0;

	private int _deleteLogsCount = 0;

	private int _logCount = 0;

	private int _initializeCount = 0;

	private int _startCount = 0;

	private int _pauseCount = 0;

	private int _resumeCount = 0;

	private int _stopCount = 0;

	private int _destroyCount = 0;

	private int _flushCount = 0;

	private int _decomposeCount = 0;

	private int _resetCount = 0;

	private int _openCount = 0;

	private int _closeCount = 0;

	private int _disposeCount = 0;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new test trim logger impl.
	 *
	 * @param aLoggerNumber the logger number
	 */
	public TestTrimLogger( int aLoggerNumber ) {
		_loggerName = Execution.toClassName() + "-" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( aLoggerNumber ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString();
	}

	/**
	 * Instantiates a new test trim logger impl.
	 *
	 * @param aLoggerName the logger name
	 */
	public TestTrimLogger( String aLoggerName ) {
		_loggerName = aLoggerName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHOD COUNTERS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the find logs count.
	 *
	 * @return the find logs count
	 */
	public int getFindLogsCount() {
		return _findLogsCount;
	}

	/**
	 * Gets the clear count.
	 *
	 * @return the clear count
	 */
	public int getClearCount() {
		return _clearCount;
	}

	/**
	 * Gets the delete logs count.
	 *
	 * @return the delete logs count
	 */
	public int getDeleteLogsCount() {
		return _deleteLogsCount;
	}

	/**
	 * Gets the log count.
	 *
	 * @return the log count
	 */
	public int getLogCount() {
		return _logCount;
	}

	/**
	 * Gets the initialize count.
	 *
	 * @return the initialize count
	 */
	public int getInitializeCount() {
		return _initializeCount;
	}

	/**
	 * Gets the start count.
	 *
	 * @return the start count
	 */
	public int getStartCount() {
		return _startCount;
	}

	/**
	 * Gets the pause count.
	 *
	 * @return the pause count
	 */
	public int getPauseCount() {
		return _pauseCount;
	}

	/**
	 * Gets the resume count.
	 *
	 * @return the resume count
	 */
	public int getResumeCount() {
		return _resumeCount;
	}

	/**
	 * Gets the stop count.
	 *
	 * @return the stop count
	 */
	public int getStopCount() {
		return _stopCount;
	}

	/**
	 * Gets the destroy count.
	 *
	 * @return the destroy count
	 */
	public int getDestroyCount() {
		return _destroyCount;
	}

	/**
	 * Gets the flush count.
	 *
	 * @return the flush count
	 */
	public int getFlushCount() {
		return _flushCount;
	}

	/**
	 * Gets the decompose count.
	 *
	 * @return the decompose count
	 */
	public int getDecomposeCount() {
		return _decomposeCount;
	}

	/**
	 * Gets the reset count.
	 *
	 * @return the reset count
	 */
	public int getResetCount() {
		return _resetCount;
	}

	/**
	 * Gets the dispose count.
	 *
	 * @return the dispose count
	 */
	public int getDisposeCount() {
		return _disposeCount;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs() {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs( int aLimit ) {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return new RecordsImpl<>( new RecordImpl<Object>() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs( Header<Object> aHeader, int aLimit ) {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return new RecordsImpl<>( new RecordImpl<Object>() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs( Criteria aCriteria ) {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return new RecordsImpl<>( new RecordImpl<Object>() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs( Criteria aCriteria, int aLimit ) {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return new RecordsImpl<>( new RecordImpl<Object>() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs( Criteria aCriteria, Header<Object> aHeader ) {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return new RecordsImpl<>( new RecordImpl<Object>() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<Object> findLogs( Criteria aCriteria, Header<Object> aHeader, int aLimit ) {
		_findLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _findLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
		return new RecordsImpl<>( new RecordImpl<Object>() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends Object> aRecord ) {
		_logCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _logCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		_clearCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _clearCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteLogs( Criteria aCriteria ) {
		_deleteLogsCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _deleteLogsCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMPONENT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		_initializeCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _initializeCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		_startCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _startCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		_pauseCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _pauseCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		_resumeCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _resumeCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		_stopCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _stopCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_destroyCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _destroyCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_flushCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _flushCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decompose() {
		_decomposeCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _decomposeCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_resetCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _resetCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_openCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _openCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		_closeCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _closeCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		_disposeCount++;
		LOGGER.debug( _loggerName + " [" + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( _disposeCount ) ).withColumnWidth( 5 ).withFillChar( '0' ).toString() + "]" );
	}
}
