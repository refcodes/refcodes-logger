// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class RuntimeLoggerFactorySingletonTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void create() {
		final RuntimeLogger theBar = RuntimeLoggerFactorySingleton.getInstance().create( "com.acme.foo.bar" );
		final RuntimeLogger theFoo = RuntimeLoggerFactorySingleton.getInstance().create( "com.acme.foo" );
		final RuntimeLogger theAcme = RuntimeLoggerFactorySingleton.getInstance().create( "com.acme" );
		final RuntimeLogger theCom = RuntimeLoggerFactorySingleton.getInstance().create( "com" );
		final RuntimeLogger theOrg = RuntimeLoggerFactorySingleton.getInstance().create( "org" );
		final RuntimeLogger theRoot = RuntimeLoggerFactorySingleton.createRuntimeLogger();
		final RuntimeLogger theDot = RuntimeLoggerFactorySingleton.getInstance().create( "." );
		final RuntimeLogger theNop = RuntimeLoggerFactorySingleton.getInstance().create( "" );
		try {
			RuntimeLoggerFactorySingleton.getInstance().create( (String) null );
			fail( "The identifier must not be null." );
		}
		catch ( IllegalArgumentException e ) { /* expected */}
		assertSame( theBar, theFoo );
		assertSame( theBar, theAcme );
		assertSame( theRoot, theCom );
		assertSame( theRoot, theOrg );
		assertSame( theRoot, theDot );
		assertSame( theRoot, theNop );
		assertNotSame( theBar, theRoot );
	}
}
