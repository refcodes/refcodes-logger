// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Collection;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.tabular.Column;

/**
 * This {@link TestPartedTrimLogger} is required to make the
 * {@link #getLoggers()} method available to the test cases.
 */
public class TestPartedTrimLogger extends PartedTrimLogger<Object, String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new test parted trim logger impl.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public TestPartedTrimLogger( Column<String> aPartitionColumn, LoggerFactory<TrimLogger<Object>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Instantiates a new test parted trim logger impl.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public TestPartedTrimLogger( Column<String> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<TrimLogger<Object>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aDefaultLoggerName, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Instantiates a new test parted trim logger impl.
	 *
	 * @param aComponentExecutionStrategy the component execution strategy
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public TestPartedTrimLogger( ExecutionStrategy aComponentExecutionStrategy, Column<String> aPartitionColumn, LoggerFactory<TrimLogger<Object>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aComponentExecutionStrategy, aPartitionColumn, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Instantiates a new test parted trim logger impl.
	 *
	 * @param aComponentExecutionStrategy the component execution strategy
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public TestPartedTrimLogger( ExecutionStrategy aComponentExecutionStrategy, Column<String> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<TrimLogger<Object>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aComponentExecutionStrategy, aPartitionColumn, aDefaultLoggerName, aLoggerFactory, isPartitionAutoInitialize );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<TrimLogger<Object>> getLoggers() {
		return super.getLoggers();
	}
}
