module org.refcodes.logger {
	requires org.refcodes.properties.ext.application;
	requires org.refcodes.data;
	requires org.refcodes.runtime;
	requires org.refcodes.textual;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.criteria;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.tabular;
	requires transitive java.logging;

	exports org.refcodes.logger;
}
