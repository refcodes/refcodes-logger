// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.function.Supplier;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.ConsoleDimension;
import org.refcodes.factory.TypeFactory;
import org.refcodes.runtime.Correlation;
import org.refcodes.textual.ColumnSetupMetrics;
import org.refcodes.textual.ColumnSetupMetricsImpl;
import org.refcodes.textual.ColumnWidthType;
import org.refcodes.textual.EscapeCodeFactory;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.MoreTextMode;
import org.refcodes.textual.SplitTextMode;
import org.refcodes.textual.TextFormatMode;

/**
 * Predefined layouts passed to logger implementation supporting
 * {@link ColumnLayout} modes.
 */
public enum ColumnLayout {

	/**
	 * Some default layout is choosen.
	 */
	NONE(ColumnLayout::toBasicColumnMetrics),

	/**
	 * All available information is to be logged with minimal information loss.
	 * A superuser or administrator might like this layout.
	 */
	SUPERUSER(ColumnLayout::toSuperuserColumnMetrics),

	/**
	 * A fallback layout trying to log most available information with taking
	 * information loss into account. You might like this in case you have a
	 * terminal width less than {@link ConsoleDimension#NORM_WIDTH}. This layout
	 * might be chosen automatically in case the column width is not sufficient
	 * for the other layouts.
	 */
	GRANDPA(ColumnLayout::toGrandpaColumnMetrics),

	/**
	 * Similar to {@link #GRANDPA} though even with more information omitted
	 * (makes each log line fit on one console line).
	 */
	BASIC(ColumnLayout::toBasicColumnMetrics),

	/**
	 * Some technical details may be omitted when logging. Focus is the logging
	 * of development targeted information (such as method names or line
	 * numbers).
	 */
	DEVELOPER(ColumnLayout::toDeveloperColumnMetrics),

	/**
	 * Some technical details may be omitted when logging, though the messages
	 * are not truncated.
	 */
	HACKER(ColumnLayout::toHackerColumnMetrics),

	/**
	 * Some technical details may be omitted when logging though the
	 * {@link Correlation#SESSION} TID and the {@link Correlation#REQUEST} TID
	 * must not be omitted. Focus is the logging of communication targeted
	 * information (such as the correlation IDs).
	 */
	DEVOPS(ColumnLayout::toDevopsColumnMetrics),

	/**
	 * Most technical details may be omitted, focus is the logging of human
	 * targeted information such a person using a command line tool which prints
	 * out some nicely formatted status information.
	 */
	ENDUSER(ColumnLayout::toEnduserColumnMetrics),

	/**
	 * All information is displayed, no information is cut off, an analyst might
	 * like it though it does not please your eyes any more.
	 */
	ANALYST(ColumnLayout::toAnalystColumnMetrics);

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String EXCEPTION = "Exception";
	private static final String MESSAGE = "Message";
	private static final String METHOD = "Method";
	private static final String CODE_LINE = "@";
	private static final String CLASS = "Class";
	private static final String REQUEST = "Request";
	private static final String SESSION = "Session";
	private static final String THREAD = "Thread";
	private static final String PRIORITY = "Level";
	private static final String DATE = "Date";
	private static final String LINE_NUMBER = "#";
	private static final String ANSI_LINE_NUMBER = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_MAGENTA, AnsiEscapeCode.DEFAULT_BACKGROUND_COLOR ); // fg( Color.MAGENTA ).bg( Color.DEFAULT )
	private static final String ANSI_DATE = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_WHITE ); // fg( Color.WHITE )
	private static final String ANSI_THREAD = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_CYAN ); // fg( Color.CYAN )
	private static final String ANSI_CORRELATION_ID = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_CYAN, AnsiEscapeCode.DEFAULT_BACKGROUND_COLOR ); // fg( Color.CYAN ).bg( Color.DEFAULT )
	private static final String ANSI_MESSAGE = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.DEFAULT_BACKGROUND_COLOR ); // .fg( Color.DEFAULT ).bg( Color.DEFAULT )
	private static final String ANSI_EXCEPTION = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_RED ); // fgBright( Color.RED )
	private static final String ANSI_CLASS = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.DEFAULT_BACKGROUND_COLOR ); // fg( Color.DEFAULT ).bg( Color.DEFAULT )
	private static final String ANSI_METHOD = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BLUE, AnsiEscapeCode.BOLD ); // bold().fg( Color.BLUE )
	private static final String ANSI_PRIORITY_WARN = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_RED, AnsiEscapeCode.BOLD ); // bold().fg( Color.RED )
	private static final String ANSI_PRIORITY_NOTICE = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_MAGENTA, AnsiEscapeCode.BOLD ); // bold().fg( Color.MAGENTA )
	private static final String ANSI_PRIORITY_INFO = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_GREEN, AnsiEscapeCode.BOLD ); // bold().fg( Color.GREEN )
	private static final String ANSI_PRIORITY_PANIC = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_WHITE, AnsiEscapeCode.BOLD, AnsiEscapeCode.BG_BRIGHT_RED ); // bold().fgBright( Color.WHITE ).bgBright( Color.RED )
	private static final String ANSI_PRIORITY_ALERT = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_YELLOW, AnsiEscapeCode.BOLD, AnsiEscapeCode.BG_BRIGHT_RED ); // bold().fgBright( Color.YELLOW ).bgBright( Color.RED )
	private static final String ANSI_PRIORITY_CRITICAL = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_YELLOW, AnsiEscapeCode.BOLD, AnsiEscapeCode.BG_BRIGHT_MAGENTA ); // bold().fgBright( Color.YELLOW ).bgBright( Color.MAGENTA )
	private static final String ANSI_PRIORITY_ERROR = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_RED, AnsiEscapeCode.BOLD ); // bold().fgBright( Color.RED )
	private static final String ANSI_PRIORITY_DEBUG = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_YELLOW, AnsiEscapeCode.BOLD, AnsiEscapeCode.BG_BRIGHT_BLUE ); // bold().fgBright( Color.YELLOW ).bgBright( Color.BLUE )
	private static final String ANSI_PRIORITY_TRACE = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_WHITE, AnsiEscapeCode.BOLD ); // bold().fg( Color.WHITE )
	private static final String ANSI_HEADER = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.BOLD, AnsiEscapeCode.DEFAULT_BACKGROUND_COLOR ); // bold().fg( Color.DEFAULT ).bg( Color.DEFAULT )

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TypeFactory<ColumnSetupMetrics[]> _columnSetupMetricsSupplier;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs this enumeration.
	 * 
	 * @param aColumnSetupMetricsSupplier The {@link Supplier} fabricating the
	 *        according metrics.
	 */
	private ColumnLayout( TypeFactory<ColumnSetupMetrics[]> aColumnSetupMetricsSupplier ) {
		_columnSetupMetricsSupplier = aColumnSetupMetricsSupplier;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates the {@link ColumnSetupMetrics} for the actual enumeration
	 * instance.
	 * 
	 * @return The {@link ColumnSetupMetrics} array in question.
	 */
	public ColumnSetupMetrics[] toColumnSetupMetrics() {
		return _columnSetupMetricsSupplier.create();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Case insensitive convenience method for converting a {@link String} to an
	 * according enumeration.
	 * 
	 * @param aValue The {@link String} to be converted.
	 * 
	 * @return The according enumeration or null if the provided value is
	 *         unknown.
	 */
	public static ColumnLayout toColumnLayout( String aValue ) {
		aValue = aValue != null ? aValue.replaceAll( "-", "" ).replaceAll( "_", "" ) : aValue;
		for ( ColumnLayout eValue : values() ) {
			if ( eValue.name().replaceAll( "-", "" ).replaceAll( "_", "" ).equalsIgnoreCase( aValue ) ) {
				return eValue;
			}
		}
		return null;
	}

	private static EscapeCodeFactory LOG_PRIORITY_ESC_CODE_FACTORY = new EscapeCodeFactory() {
		@Override
		public String create( Object aIdentifier ) {
			if ( aIdentifier != null ) {
				String theIdentifier = aIdentifier instanceof String ? (String) aIdentifier : aIdentifier.toString();
				if ( LogPriority.PANIC.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_PANIC;
				}
				if ( LogPriority.ALERT.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_ALERT;
				}
				if ( LogPriority.CRITICAL.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_CRITICAL;
				}
				if ( LogPriority.ERROR.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_ERROR;
				}
				if ( LogPriority.WARN.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_WARN;
				}
				if ( LogPriority.NOTICE.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_NOTICE;
				}
				if ( LogPriority.INFO.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_INFO;
				}
				if ( LogPriority.DEBUG.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_DEBUG;
				}
				if ( LogPriority.TRACE.name().equals( theIdentifier ) ) {
					return ANSI_PRIORITY_TRACE;
				}
			}
			return null;
		}
	};

	// @formatter:off
	private static final ColumnSetupMetrics[] toSuperuserColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl( 10, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_LINE_NUMBER ).withRowMoreTextMode( MoreTextMode.NONE ).withName(LINE_NUMBER),
			/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE),
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY),
			/* Thread:      */ new ColumnSetupMetricsImpl( 10, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_THREAD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(THREAD),
			/* Session:     */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCode( ANSI_CORRELATION_ID ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(SESSION),
			/* Request:     */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCode( ANSI_CORRELATION_ID ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(REQUEST),
			/* Class:       */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode(ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(CLASS),
			/* Code line:   */ new ColumnSetupMetricsImpl(  4, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(CODE_LINE),
			/* Method:      */ new ColumnSetupMetricsImpl( 30, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_METHOD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(METHOD),
			/* Message:     */ new ColumnSetupMetricsImpl(  2, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_MESSAGE ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(MESSAGE),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on

	// @formatter:off
	private static final ColumnSetupMetrics[] toBasicColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl( 5, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_LINE_NUMBER ).withRowMoreTextMode( MoreTextMode.NONE ).withName(LINE_NUMBER),
			/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE),
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY),
			/* Thread:      */ new ColumnSetupMetricsImpl( 8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_THREAD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(THREAD),
			/* Session:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Request:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Class:       */ new ColumnSetupMetricsImpl().withHide(),
			/* Code line:   */ new ColumnSetupMetricsImpl().withHide(),
			/* Method:      */ new ColumnSetupMetricsImpl().withHide(),
			/* Message:     */ new ColumnSetupMetricsImpl(  2, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_MESSAGE ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(MESSAGE),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on

	// @formatter:off
	private static final ColumnSetupMetrics[] toGrandpaColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl().withHide(), 
			/* Date:        */ new ColumnSetupMetricsImpl(  19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( DATE), 
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY),
			/* Thread:      */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_THREAD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( THREAD ), 
			/* Session:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Request:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Class:       */ new ColumnSetupMetricsImpl().withHide(), 
			/* Code line:   */ new ColumnSetupMetricsImpl().withHide(),
			/* Method:      */ new ColumnSetupMetricsImpl().withHide(),
			/* Message:     */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_MESSAGE ).withRowMoreTextMode( MoreTextMode.NONE ).withName( MESSAGE ),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on

	// @formatter:off
	private static final ColumnSetupMetrics[] toDeveloperColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl().withHide(), 
			/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE), 
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY), 
			/* Thread:      */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_THREAD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( THREAD ),
			/* Session:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Request:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Class:       */ new ColumnSetupMetricsImpl(  3, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( CLASS ), 
			/* Code line:   */ new ColumnSetupMetricsImpl(  4, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(CODE_LINE),
			/* Method:      */ new ColumnSetupMetricsImpl(  2, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_METHOD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( METHOD ),
			/* Message:     */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_MESSAGE ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( MESSAGE ),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on

	// @formatter:off
		private static final ColumnSetupMetrics[] toHackerColumnMetrics() {
			return new ColumnSetupMetrics[] {
				/* Line number: */ new ColumnSetupMetricsImpl().withHide(), 
				/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE), 
				/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY), 
				/* Thread:      */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_THREAD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( THREAD ),
				/* Session:     */ new ColumnSetupMetricsImpl().withHide(),
				/* Request:     */ new ColumnSetupMetricsImpl().withHide(),
				/* Class:       */ new ColumnSetupMetricsImpl(  3, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( CLASS ), 
				/* Code line:   */ new ColumnSetupMetricsImpl(  4, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(CODE_LINE),
				/* Method:      */ new ColumnSetupMetricsImpl(  2, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_METHOD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( METHOD ),
				/* Message:     */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_MESSAGE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName(MESSAGE),
				/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
			};
		}
		// @formatter:on

	// @formatter:off
	private static final ColumnSetupMetrics[] toDevopsColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl().withHide(), 
			/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE), 
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY), 
			/* Thread:      */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_THREAD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( THREAD ),
			/* Session:     */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_CORRELATION_ID).withRowMoreTextMode( MoreTextMode.LEFT ).withName( SESSION),
			/* Request:     */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCode( ANSI_CORRELATION_ID ).withRowMoreTextMode( MoreTextMode.LEFT ).withName(REQUEST),
			/* Class:       */ new ColumnSetupMetricsImpl(  3, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_CLASS ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( CLASS ), 
			/* Code line:   */ new ColumnSetupMetricsImpl().withHide(),
			/* Method:      */ new ColumnSetupMetricsImpl(  2, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_METHOD ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( METHOD ),
			/* Message:     */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_MESSAGE ).withRowMoreTextMode( MoreTextMode.LEFT ).withName( MESSAGE ),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on

	// @formatter:off
	private static final ColumnSetupMetrics[] toEnduserColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl().withHide(), 
			/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE), 
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY), 
			/* Thread:      */ new ColumnSetupMetricsImpl().withHide(),
			/* Session:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Request:     */ new ColumnSetupMetricsImpl().withHide(),
			/* Class:       */ new ColumnSetupMetricsImpl().withHide(), 
			/* Code line:   */ new ColumnSetupMetricsImpl().withHide(),
			/* Method:      */ new ColumnSetupMetricsImpl().withHide(),
			/* Message:     */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_MESSAGE ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_SPACE ).withName( MESSAGE ),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on

	// @formatter:off
	private static final ColumnSetupMetrics[] toAnalystColumnMetrics() {
		return new ColumnSetupMetrics[] {
			/* Line number: */ new ColumnSetupMetricsImpl( 10, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_LINE_NUMBER ).withRowMoreTextMode( MoreTextMode.NONE ).withName(LINE_NUMBER),
			/* Date:        */ new ColumnSetupMetricsImpl( 19, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_DATE ).withRowMoreTextMode( MoreTextMode.NONE ).withName(DATE),
			/* Priority:    */ new ColumnSetupMetricsImpl(  7, ColumnWidthType.ABSOLUTE ).withTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER).withRowEscapeCodeFactory( LOG_PRIORITY_ESC_CODE_FACTORY ).withRowMoreTextMode( MoreTextMode.NONE ).withName(PRIORITY),
			/* Thread:      */ new ColumnSetupMetricsImpl( 10, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_THREAD ).withRowSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withName(THREAD),
			/* Session:     */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCode( ANSI_CORRELATION_ID ).withRowSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withName(SESSION),
			/* Request:     */ new ColumnSetupMetricsImpl(  8, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.CENTER ).withRowEscapeCode( ANSI_CORRELATION_ID ).withRowSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withName(REQUEST),
			/* Class:       */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode(ANSI_CLASS ).withRowSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withName(CLASS),
			/* Code line:   */ new ColumnSetupMetricsImpl(  4, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withRowEscapeCode( ANSI_CLASS ).withRowSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withName(CODE_LINE),
			/* Method:      */ new ColumnSetupMetricsImpl( 30, ColumnWidthType.ABSOLUTE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_METHOD ).withRowSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withName(METHOD),
			/* Message:     */ new ColumnSetupMetricsImpl(  2, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT ).withRowEscapeCode( ANSI_MESSAGE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName(MESSAGE),
			/* Exception:   */ new ColumnSetupMetricsImpl(  1, ColumnWidthType.RELATIVE ).withHeaderTextFormatMode( TextFormatMode.CELL ).withHeaderHorizAlignTextMode( HorizAlignTextMode.CENTER ).withHeaderEscapeCode( ANSI_HEADER ).withHeaderMoreTextMode( MoreTextMode.RIGHT ).withRowHorizAlignTextMode( HorizAlignTextMode.LEFT).withRowEscapeCode( ANSI_EXCEPTION ).withRowMoreTextMode( MoreTextMode.NONE ).withRowSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withName( EXCEPTION )
		};
	}
	// @formatter:on
}
