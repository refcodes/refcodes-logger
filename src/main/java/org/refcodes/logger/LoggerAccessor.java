// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

/**
 * Provides an accessor for a {@link Logger} property.
 *
 * @param <L> The {@link Logger}'s subclass to be accessed.
 */
public interface LoggerAccessor<L extends Logger<?>> {

	/**
	 * Retrieves the logger from the logger property.
	 * 
	 * @return The logger stored by the logger property.
	 */
	L getLogger();

	/**
	 * Provides a mutator for a {@link Logger} property.
	 * 
	 * @param <L> The {@link Logger}'s subclass to be accessed.
	 */
	public interface LoggerMutator<L extends Logger<?>> {

		/**
		 * Sets the logger for the logger property.
		 * 
		 * @param aLogger The logger to be stored by the logger property.
		 */
		void setLogger( L aLogger );
	}

	/**
	 * Provides a {@link Logger} property.
	 * 
	 * @param <L> The {@link Logger}'s subclass to be accessed.
	 */
	public interface LoggerProperty<L extends Logger<?>> extends LoggerAccessor<L>, LoggerMutator<L> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setLogger(Logger)} and returns the very same value (getter).
		 * 
		 * @param aLogger The value to set (via {@link #setLogger(Logger)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default L letLogger( L aLogger ) {
			setLogger( aLogger );
			return aLogger;
		}
	}
}
