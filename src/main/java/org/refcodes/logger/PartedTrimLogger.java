// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.io.IOException;

import org.refcodes.component.ComponentComposite;
import org.refcodes.component.ComponentUtility;
import org.refcodes.component.InitializeException;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.Record;

/**
 * The {@link PartedTrimLogger} is a ready to use implementation of a parted
 * {@link TrimLogger} extending the {@link AbstractPartedTrimLogger}.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * @param <P> The type of the {@link Column}'s value used for partitioning the
 *        {@link Logger}.
 */
public class PartedTrimLogger<T, P extends T> extends AbstractPartedTrimLogger<TrimLogger<T>, T, P> implements ComponentComposite {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutionStrategy _componentExecutionStrategy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link PartedTrimLogger} with the according
	 * parameters.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this composite
	 *        with partitions.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public PartedTrimLogger( Column<P> aPartitionColumn, LoggerFactory<TrimLogger<T>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		this( ExecutionStrategy.JOIN, aPartitionColumn, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Instantiates a new {@link PartedTrimLogger} with the according
	 * parameters.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this composite
	 *        with partitions.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public PartedTrimLogger( Column<P> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<TrimLogger<T>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		this( ExecutionStrategy.JOIN, aPartitionColumn, aDefaultLoggerName, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Similar to the {@link #PartedTrimLogger(Column, LoggerFactory, boolean)}
	 * constructor with the additional option of determining the execution
	 * strategy of the state change request calls for the encapsulated
	 * {@link TrimLogger} instances (as of {@link ComponentComposite}).
	 *
	 * @param aComponentExecutionStrategy The strategy to be used when invoking
	 *        a component's (encapsulated {@link TrimLogger} instance's) state
	 *        change request call (as of {@link ComponentComposite}).
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this composite
	 *        with partitions.
	 * @param isPartitionAutoInitialize the is partition auto initialize
	 */
	public PartedTrimLogger( ExecutionStrategy aComponentExecutionStrategy, Column<P> aPartitionColumn, LoggerFactory<TrimLogger<T>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aLoggerFactory, isPartitionAutoInitialize );
		_componentExecutionStrategy = aComponentExecutionStrategy;
	}

	/**
	 * Similar to the {@link #PartedTrimLogger(Column, LoggerFactory, boolean)}
	 * constructor with the additional option of determining the execution
	 * strategy of the state change request calls for the encapsulated
	 * {@link TrimLogger} instances (as of {@link ComponentComposite}).
	 *
	 * @param aComponentExecutionStrategy The strategy to be used when invoking
	 *        a component's (encapsulated {@link TrimLogger} instance's) state
	 *        change request call (as of {@link ComponentComposite}).
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this composite
	 *        with partitions.
	 * @param isPartitionAutoInitialize the is partition auto initialize
	 */
	public PartedTrimLogger( ExecutionStrategy aComponentExecutionStrategy, Column<P> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<TrimLogger<T>> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aDefaultLoggerName, aLoggerFactory, isPartitionAutoInitialize );
		_componentExecutionStrategy = aComponentExecutionStrategy;
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMPONENT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		ComponentUtility.initialize( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		ComponentUtility.start( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		ComponentUtility.pause( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		ComponentUtility.resume( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		ComponentUtility.stop( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decompose() {
		ComponentUtility.decompose( _componentExecutionStrategy, getLoggers() );
		getLoggers().clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		ComponentUtility.flush( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		ComponentUtility.destroy( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		ComponentUtility.reset( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		ComponentUtility.open( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		ComponentUtility.close( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ComponentUtility.dispose( _componentExecutionStrategy, getLoggers() );
	}
}
