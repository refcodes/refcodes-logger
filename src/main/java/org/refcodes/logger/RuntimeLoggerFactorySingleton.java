// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger;

import java.util.Map;

import org.refcodes.runtime.Execution;

/**
 * The singleton of the {@link RuntimeLoggerFactoryImpl} for easy
 * {@link RuntimeLogger} creation. See {@link RuntimeLoggerFactoryImpl} on how
 * to set up your "<code>runtimelogger-config.xml</code>" for your individual
 */
public class RuntimeLoggerFactorySingleton extends RuntimeLoggerFactoryImpl {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static RuntimeLoggerFactorySingleton _runtimeLoggerFactorySingleton;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new runtime logger factory singleton.
	 */
	protected RuntimeLoggerFactorySingleton() {}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link RuntimeLoggerFactorySingleton}.
	 * 
	 * @return The {@link RuntimeLoggerFactory} singleton's instance.
	 */
	public static RuntimeLoggerFactory getInstance() {
		if ( _runtimeLoggerFactorySingleton == null ) {
			synchronized ( RuntimeLoggerFactorySingleton.class ) {
				_runtimeLoggerFactorySingleton = new RuntimeLoggerFactorySingleton();
			}
		}
		return _runtimeLoggerFactorySingleton;
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create()}.
	 *
	 * @return the runtime logger
	 */
	public static RuntimeLogger createRuntimeLogger() {
		return getInstance().create();
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create(Map)}.
	 *
	 * @param aProperties the properties
	 * 
	 * @return the runtime logger
	 */
	public static RuntimeLogger createRuntimeLogger( Map<String, String> aProperties ) {
		return getInstance().create( aProperties );
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create(Map)}.
	 *
	 * @param aIdentifier the identifier
	 * 
	 * @return the runtime logger
	 */
	public static RuntimeLogger createRuntimeLogger( String aIdentifier ) {
		return getInstance().create( aIdentifier );
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create(Object, Map)}.
	 *
	 * @param aIdentifier the identifier
	 * @param aProperties the properties
	 * 
	 * @return the runtime logger
	 */
	public RuntimeLogger createRuntimeLogger( String aIdentifier, Map<String, String> aProperties ) {
		return getInstance().create( aIdentifier, aProperties );
	}

	/**
	 * Initializes the {@link RuntimeLogger} functionality, e.g. binds the JUL
	 * framework the the {@link RuntimeLogger} toolkit.
	 */
	public static void bindJavaUtilLogging() {
		Execution.setJulLoggingHandler( new RuntimeLoggerHandler() );
	}
}
