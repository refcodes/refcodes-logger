// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.Records;

/**
 * The {@link QueryLogger} extends the {@link Logger}; providing additional
 * functionality for querying once logged data {@link Records} by specifying
 * query {@link Criteria} or other query restrictions.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public interface QueryLogger<T> extends Logger<T> {

	/**
	 * Retrieves all available {@link Record} instances being logged.
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged.
	 */
	Records<T> findLogs();

	/**
	 * Retrieves all available {@link Record} instances being logged matching
	 * the given criteria and restrictions.
	 * 
	 * @param aLimit The maximum {@link Record} instances contained in the
	 *        returned {@link Records} instance; there may be more
	 *        {@link Record} instances which are not contained in the
	 *        {@link Records} instance in case the {@link Records} instance's
	 *        size is that of the specified limit.
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged matching the given criteria
	 *         and restrictions.
	 */
	Records<T> findLogs( int aLimit );

	/**
	 * Retrieves all available {@link Record} instances being logged matching
	 * the given criteria and restrictions.
	 * 
	 * @param aHeader The {@link Header} used to restrict the "columns"
	 *        (key/value entries) in the retrieved {@link Record} instances
	 *        (provided by the {@link Records} instance).
	 * @param aLimit The maximum {@link Record} instances contained in the
	 *        returned {@link Records} instance; there may be more
	 *        {@link Record} instances which are not contained in the
	 *        {@link Records} instance in case the {@link Records} instance's
	 *        size is that of the specified limit.
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged matching the given criteria
	 *         and restrictions.
	 */
	Records<T> findLogs( Header<T> aHeader, int aLimit );

	/**
	 * Retrieves all available {@link Record} instances being logged matching
	 * the given criteria and restrictions.
	 * 
	 * @param aCriteria The {@link Criteria} to be applied to the {@link Record}
	 *        instances
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged matching the given criteria
	 *         and restrictions.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	Records<T> findLogs( Criteria aCriteria ) throws BadCriteriaException;

	/**
	 * Retrieves all available {@link Record} instances being logged matching
	 * the given criteria and restrictions.
	 * 
	 * @param aCriteria The {@link Criteria} to be applied to the {@link Record}
	 *        instances
	 * @param aLimit The maximum {@link Record} instances contained in the
	 *        returned {@link Records} instance; there may be more
	 *        {@link Record} instances which are not contained in the
	 *        {@link Records} instance in case the {@link Records} instance's
	 *        size is that of the specified limit.
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged matching the given criteria
	 *         and restrictions.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	Records<T> findLogs( Criteria aCriteria, int aLimit ) throws BadCriteriaException;

	/**
	 * Retrieves all available {@link Record} instances being logged matching
	 * the given criteria and restrictions.
	 * 
	 * @param aCriteria The {@link Criteria} to be applied to the {@link Record}
	 *        instances
	 * @param aHeader The {@link Header} used to restrict the "columns"
	 *        (key/value entries) in the retrieved {@link Record} instances
	 *        (provided by the {@link Records} instance).
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged matching the given criteria
	 *         and restrictions.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	Records<T> findLogs( Criteria aCriteria, Header<T> aHeader ) throws BadCriteriaException;

	/**
	 * Retrieves all available {@link Record} instances being logged matching
	 * the given criteria and restrictions.
	 * 
	 * @param aCriteria The {@link Criteria} to be applied to the {@link Record}
	 *        instances
	 * @param aHeader The {@link Header} used to restrict the "columns"
	 *        (key/value entries) in the retrieved {@link Record} instances
	 *        (provided by the {@link Records} instance).
	 * @param aLimit The maximum {@link Record} instances contained in the
	 *        returned {@link Records} instance; there may be more
	 *        {@link Record} instances which are not contained in the
	 *        {@link Records} instance in case the {@link Records} instance's
	 *        size is that of the specified limit.
	 * 
	 * @return A {@link Records} instance containing all available
	 *         {@link Record} instances being logged matching the given criteria
	 *         and restrictions.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	Records<T> findLogs( Criteria aCriteria, Header<T> aHeader, int aLimit ) throws BadCriteriaException;

}
