// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.refcodes.logger.LogPriorityAccessor.LogPriorityProperty;
import org.refcodes.logger.LoggerAccessor.LoggerProperty;
import org.refcodes.mixin.NameAccessor.NameProperty;
import org.refcodes.runtime.Correlation;
import org.refcodes.runtime.Execution;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordImpl;
import org.refcodes.textual.MessageBuilder;

/**
 * The default implementation of the {@link RuntimeLogger} interface to be
 * configured with a (back-end) {@link Logger}. In case no (back-end)
 * {@link Logger} has been set via {@link #setLogger(Logger)} then the plain
 * {@link SystemLogger} will be used as fallback.
 */
public class RuntimeLoggerImpl implements RuntimeLogger, LoggerProperty<Logger<Object>>, NameProperty, LogPriorityProperty {

	// /////////////////////////////////////////////////////////////////////////
	// Constants:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ROOT_LOGGER_NAME = "*";
	public static final LogPriority DEFAULT_LOG_PRIORITY = LogPriority.INFO;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;
	private Logger<Object> _logger;
	private LogPriority _logPriority;
	static AtomicInteger _lineNumber = new AtomicInteger( 1 );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link RuntimeLogger} logging with the given {@link Logger}
	 * and the desired {@link LogPriority} for the messages being logged and
	 * upwards (from the given {@link LogPriority#getPriority()} and higher).
	 *
	 * @param aName the name
	 * @param aLogger The logger used for logging the runtime logs.
	 * @param aPriority The {@link LogPriority} specifying which log messages
	 *        are to be logged. Any log aMessage with a {@link LogPriority} as
	 *        the given one or higher (regarding the
	 *        {@link LogPriority#getPriority()} value) is being logged.
	 */
	public RuntimeLoggerImpl( String aName, Logger<Object> aLogger, LogPriority aPriority ) {
		_logger = aLogger;
		_logPriority = aPriority;
		_name = aName;
		if ( aLogger instanceof NameMutator ) {
			( (NameMutator) aLogger ).setName( aName );
		}
	}

	/**
	 * Constructs a {@link RuntimeLogger} logging with the given {@link Logger}
	 * and the desired {@link LogPriority} for the messages being logged and
	 * upwards (from the given {@link LogPriority#getPriority()} and higher).
	 *
	 * @param aName the name
	 * @param aLogger The logger used for logging the runtime logs.
	 */
	public RuntimeLoggerImpl( String aName, Logger<Object> aLogger ) {
		this( aName, aLogger, DEFAULT_LOG_PRIORITY );
	}

	/**
	 * Constructs a {@link RuntimeLogger} logging with the given {@link Logger}
	 * and the desired {@link LogPriority} for the messages being logged and
	 * upwards (from the given {@link LogPriority#getPriority()} and higher).
	 * 
	 * @param aLogger The logger used for logging the runtime logs.
	 * @param aPriority The {@link LogPriority} specifying which log messages
	 *        are to be logged. Any log aMessage with a {@link LogPriority} as
	 *        the given one or higher (regarding the
	 *        {@link LogPriority#getPriority()} value) is being logged.
	 */
	public RuntimeLoggerImpl( Logger<Object> aLogger, LogPriority aPriority ) {
		this( ROOT_LOGGER_NAME, aLogger, aPriority );
	}

	/**
	 * Constructs a {@link RuntimeLogger} logging with the given {@link Logger}
	 * and the desired {@link LogPriority} for the messages being logged and
	 * upwards (from the given {@link LogPriority#getPriority()} and higher).
	 * 
	 * @param aLogger The logger used for logging the runtime logs.
	 */
	public RuntimeLoggerImpl( Logger<Object> aLogger ) {
		this( ROOT_LOGGER_NAME, aLogger, DEFAULT_LOG_PRIORITY );
	}

	/**
	 * Constructs a {@link RuntimeLogger} logging with the given {@link Logger}
	 * and the desired {@link LogPriority} for the messages being logged and
	 * upwards (from the given {@link LogPriority#getPriority()} and higher).
	 */
	public RuntimeLoggerImpl() {
		this( ROOT_LOGGER_NAME, new SystemLogger(), DEFAULT_LOG_PRIORITY );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( aPriority != LogPriority.DISCARD && _logPriority != LogPriority.DISCARD && isLog( aPriority ) ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( aPriority, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Throwable aThrowable ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( aPriority != LogPriority.DISCARD && _logPriority != LogPriority.DISCARD && isLog( aPriority ) ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( aPriority, aMessage, theStackTraceElement, aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLog( LogPriority aPriority ) {
		return aPriority.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LogPriority getLogPriority() {
		return _logPriority;
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogTrace() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.TRACE, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogTrace() {
		return LogPriority.TRACE.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogDebug() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.DEBUG, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogDebug() {
		return LogPriority.DEBUG.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogInfo() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.INFO, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogInfo() {
		return LogPriority.INFO.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notice( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogNotice() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.NOTICE, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogNotice() {
		return LogPriority.NOTICE.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogWarn() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.WARN, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Throwable aThrowable ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogWarn() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.WARN, aMessage, theStackTraceElement, aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogWarn() {
		return LogPriority.WARN.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogError() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.ERROR, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Throwable aThrowable ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogError() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.ERROR, aMessage, theStackTraceElement, aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogError() {
		return LogPriority.ERROR.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogCritical() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.CRITICAL, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Throwable aThrowable ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogCritical() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.CRITICAL, aMessage, theStackTraceElement, aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogCritical() {
		return LogPriority.CRITICAL.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogAlert() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.ALERT, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Throwable aThrowable ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogAlert() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.ALERT, aMessage, theStackTraceElement, aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogAlert() {
		return LogPriority.ALERT.getPriority() >= _logPriority.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogPanic() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.PANIC, aMessage, theStackTraceElement, null );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Throwable aThrowable ) {
		// ---------------------------------------------------------------------
		// Keep this condition check outside the "doLog" method to prevent
		// unnecessary "StackTraceElement" instances being created:
		// ---------------------------------------------------------------------
		if ( _logPriority != LogPriority.DISCARD && isLogPanic() ) {
			final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
			doLog( LogPriority.PANIC, aMessage, theStackTraceElement, aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogPanic() {
		return LogPriority.PANIC.getPriority() >= _logPriority.getPriority();
	}

	// /////////////////////////////////////////////////////////////////////////
	// FORMAT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Object... aArguments ) {
		if ( isLog( aPriority ) ) {
			log( aPriority, MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Throwable aThrowable, Object... aArguments ) {
		if ( isLog( aPriority ) ) {
			log( aPriority, MessageBuilder.asMessage( aMessage, aArguments ), aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String aMessage, Object... aArguments ) {
		if ( isLogTrace() ) {
			trace( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String aMessage, Object... aArguments ) {
		if ( isLogDebug() ) {
			debug( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String aMessage, Object... aArguments ) {
		if ( isLogInfo() ) {
			info( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notice( String aMessage, Object... aArguments ) {
		if ( isLogNotice() ) {
			notice( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Object... aArguments ) {
		if ( isLogWarn() ) {
			warn( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Throwable aThrowable, Object... aArguments ) {
		if ( isLogWarn() ) {
			warn( MessageBuilder.asMessage( aMessage, aArguments ), aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Object... aArguments ) {
		if ( isLogError() ) {
			error( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Throwable aThrowable, Object... aArguments ) {
		if ( isLogError() ) {
			error( MessageBuilder.asMessage( aMessage, aArguments ), aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Object... aArguments ) {
		if ( isLogCritical() ) {
			critical( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Throwable aThrowable, Object... aArguments ) {
		if ( isLogCritical() ) {
			critical( MessageBuilder.asMessage( aMessage, aArguments ), aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Object... aArguments ) {
		if ( isLogAlert() ) {
			alert( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Throwable aThrowable, Object... aArguments ) {
		if ( isLogAlert() ) {
			alert( MessageBuilder.asMessage( aMessage, aArguments ), aThrowable );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Object... aArguments ) {
		if ( isLogPanic() ) {
			panic( MessageBuilder.asMessage( aMessage, aArguments ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Throwable aThrowable, Object... aArguments ) {
		if ( isLogPanic() ) {
			panic( MessageBuilder.asMessage( aMessage, aArguments ), aThrowable );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONFIGURATION:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLogger( Logger<Object> aLogger ) {
		_logger = aLogger;
	}

	/**
	 * Similar to {@link #setLogPriority(LogPriority)} with the difference, that
	 * a {@link String} is passed instead of a {@link LogPriority} element. The
	 * {@link String} variant is required by the configuration framework as used
	 * by the {@link RuntimeLoggerSingleton}.
	 * 
	 * @param aLogLevel The {@link String} representation of the required
	 *        {@link LogPriority}.
	 */
	public void setLogLevel( String aLogLevel ) {
		setLogPriority( LogPriority.valueOf( aLogLevel ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLogPriority( LogPriority aLogPriority ) {
		Execution.setJulLoggingLevel( aLogPriority.toLevel() );
		_logPriority = aLogPriority;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * Propagate the name where possible to the underlying logger. {@inheritDoc}
	 */
	@Override
	public void setName( String aName ) {
		_name = aName;
		final Logger<?> theLogger = _logger;
		if ( theLogger instanceof NameMutator ) {
			( (NameMutator) theLogger ).setName( aName );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHead() {
		_logger.printHead();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSeparator() {
		_logger.printSeparator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printTail() {
		_logger.printTail();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Logger<Object> getLogger() {
		return _logger;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method to log the required information.
	 *
	 * @param aPriority the priority
	 * @param aMessage The aMessage to be logged.
	 * @param aStackTraceElement The stack trace element to be logged.
	 * @param aThrowable The {@link Exception} to be logged.
	 */
	protected void doLog( LogPriority aPriority, String aMessage, StackTraceElement aStackTraceElement, Throwable aThrowable ) {
		final Record<Object> theRecord = new RecordImpl<>();
		theRecord.put( LoggerField.LOG_LINE_NUMBER.getKey(), _lineNumber.getAndIncrement() );
		theRecord.put( LoggerField.LOG_DATE.getKey(), new Date() );
		theRecord.put( LoggerField.LOG_PRIORITY.getKey(), aPriority );
		theRecord.put( LoggerField.LOG_THREAD_NAME.getKey(), Thread.currentThread().getName() );
		theRecord.put( LoggerField.LOG_SESSION_ID.getKey(), Correlation.SESSION.getId() );
		theRecord.put( LoggerField.LOG_REQUEST_ID.getKey(), Correlation.REQUEST.getId() );
		theRecord.put( LoggerField.LOG_FULLY_QUALIFIED_CLASS_NAME.getKey(), Execution.toFullyQualifiedClassName( aStackTraceElement ) );
		theRecord.put( LoggerField.LOG_CLASS_LINE_NUMBER.getKey(), aStackTraceElement.getLineNumber() );
		theRecord.put( LoggerField.LOG_METHODE_NAME.getKey(), Execution.toMethodName( aStackTraceElement ) );
		theRecord.put( LoggerField.LOG_MESSAGE.getKey(), aMessage );
		theRecord.put( LoggerField.LOG_EXPLODED_STACKTRACE_EXCEPTION.getKey(), aThrowable );
		_logger.log( theRecord );
	}

	/**
	 * Returns the stack trace element belonging to the caller of the caller of
	 * this method. When you use this method in your code, you get the stack
	 * trace element of the method calling your method.
	 * 
	 * @return The stack element of the caller of the caller of this method.
	 */
	public static StackTraceElement getCallerStackTraceElement() {
		final StackTraceElement[] theStackTraceElements = Thread.currentThread().getStackTrace();
		for ( StackTraceElement eStackTraceElement : theStackTraceElements ) {
			if ( !isSkipStackTraceElement( eStackTraceElement ) ) {
				return eStackTraceElement;
			}
		}
		return theStackTraceElements[theStackTraceElements.length - 1];
		// @formatter:off
		//	String eClassName = RuntimeLoggerImpl.class.getName();
		//	StackTraceElement eStackTraceElement;
		//	do {
		//		eStackTraceElement = Execution.getCallerStackTraceElement( eClassName );
		//		if ( eStackTraceElement == null ) return null;
		//		eClassName = eStackTraceElement.getClassName();
		//	} while ( isSkipStackTraceElement( eStackTraceElement ) );
		//	return eStackTraceElement;
		// @formatter:on
	}

	/**
	 * Determines whether the given {@link StackTraceElement} is not relevant
	 * determining you as the caller as it most probably originates from some
	 * logging framework.
	 *
	 * @param eStackTraceElement The {@link StackTraceElement} to analyze
	 *        whether it is to be skipped or not.
	 * 
	 * @return The in case you better skip this one as it seems to belong to
	 *         some framework reflection annotation magic.
	 */
	private static boolean isSkipStackTraceElement( StackTraceElement eStackTraceElement ) {
		final String eClassName = eStackTraceElement.getClassName();
		final String eMethodName = eStackTraceElement.getMethodName();
		// @formatter:off
		return (!eClassName.endsWith( "Test" )) &&
			(!eClassName.endsWith( "IT" )) &&
				(eClassName.equals( Thread.class.getName() ) || 
				eClassName.equals( RuntimeLoggerImpl.class.getName() ) || 
				eClassName.contains( "logger" ) || 
				eClassName.contains( "logging" ) ||
				eClassName.contains( "slf4j" ) || 
				eClassName.contains( "logback" ) ||
				eClassName.startsWith( "java.lang.reflect." ) || 
				eClassName.startsWith( "sun.reflect." ) || 
				eClassName.contains( ".log." ) || 
				"log".equals( eMethodName ) || 
				"invoke".equals( eMethodName ) ||
				"invoke0".equals( eMethodName ) ||
				"invoke1".equals( eMethodName ) ||
				"invoke2".equals( eMethodName ) ||
				"invoke3".equals( eMethodName ) ||
				"invoke4".equals( eMethodName ) ||
				"invoke5".equals( eMethodName ));
		// @formatter:on
	}
}
