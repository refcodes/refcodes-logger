// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.text.ParseException;

import org.refcodes.tabular.AbstractColumn;
import org.refcodes.tabular.Column;

/**
 * Implementation of a {@link Column} for working with {@link String} instances
 * with the semantics of a method name, being {@link Cloneable}.
 */
public class MethodNameColumnImpl extends AbstractColumn<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String METHOD_SUFFIX = "()";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link MethodNameColumnImpl} managing {@link String} instances.
	 * 
	 * @param aKey The key for the {@link MethodNameColumnImpl}.
	 */
	public MethodNameColumnImpl( String aKey ) {
		super( aKey, String.class );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized String[] toStorageStrings( String aValue ) {
		if ( !aValue.endsWith( METHOD_SUFFIX ) ) {
			return new String[] { aValue + METHOD_SUFFIX };
		}
		return new String[] { aValue };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized String fromStorageStrings( String[] aStringValues ) throws ParseException {

		if ( aStringValues == null || aStringValues.length == 0 ) {
			return null;
		}
		else if ( aStringValues.length == 1 ) {
			final String theStringValue = aStringValues[0];
			if ( !theStringValue.endsWith( METHOD_SUFFIX ) ) {
				return theStringValue;
			}
			return theStringValue.substring( 0, theStringValue.length() - METHOD_SUFFIX.length() );
		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + aStringValues.length + "> whereas only one element is being expected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable( String aValue ) {
		return super.toPrintable( aValue ) + METHOD_SUFFIX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}