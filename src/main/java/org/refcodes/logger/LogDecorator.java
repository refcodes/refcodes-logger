// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

/**
 * The {@link LogDecorator} provides means to beautify or enrich logs for
 * example when printing user friendly logs to the console. Calling the methods
 * of this decorator might have no effect, depending on the logger implementing
 * these methods. This is because decorating information adds no additional
 * information value than just beautifying log output.
 */
public interface LogDecorator {

	/**
	 * Prints a separator line in case the underlying {@link Logger} supports
	 * the such. A separator line may be printed just as of beautifying purposes
	 * and does not belong to the actual log's data. Therefore this method may
	 * just do nothing, depending on the implementing class.
	 */
	default void printSeparator() {}

	/**
	 * Prints the logger's head in case it hasn't been printed before or
	 * {@link #printTail()} has been called. Printing the head might result in
	 * printing out the additional logger's information such as the column
	 * names.
	 */
	default void printHead() {}

	/**
	 * Prints the logger's tail in case it hasn't been printed before or
	 * {@link #printHead()} has been called. Printing the tail might result in
	 * printing out the additional logger's information or visually finishing
	 * the current log sequence.
	 */
	default void printTail() {}

}
