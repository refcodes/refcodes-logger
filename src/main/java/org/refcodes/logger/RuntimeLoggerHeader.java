// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.tabular.Column;
import org.refcodes.tabular.HeaderImpl;
import org.refcodes.tabular.PrintStackTrace;

/**
 * The Class RuntimeLoggerHeader.
 */
public class RuntimeLoggerHeader extends HeaderImpl<Object> {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new runtime logger header impl.
	 */
	public RuntimeLoggerHeader() {
		this( PrintStackTrace.COMPACT );
	}

	/**
	 * Instantiates a new runtime logger header impl.
	 *
	 * @param aPrintStackTrace the print stack trace
	 */
	public RuntimeLoggerHeader( PrintStackTrace aPrintStackTrace ) {
		// @formatter:off
		super( 
			LoggerField.LOG_LINE_NUMBER.getColumn(),
			LoggerField.LOG_DATE.getColumn(),
			LoggerField.LOG_PRIORITY.getColumn(),
			LoggerField.LOG_THREAD_NAME.getColumn(),
			LoggerField.LOG_SESSION_ID.getColumn(),
			LoggerField.LOG_REQUEST_ID.getColumn(),
			LoggerField.LOG_FULLY_QUALIFIED_CLASS_NAME.getColumn(),
			LoggerField.LOG_CLASS_LINE_NUMBER.getColumn(),
			LoggerField.LOG_METHODE_NAME.getColumn(),
			LoggerField.LOG_MESSAGE.getColumn(),
			LoggerField.toExceptionLoggerField( aPrintStackTrace ).getColumn()
		);
		// @formatter:on
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the prints the stack trace.
	 *
	 * @param aPrintStackTrace the new prints the stack trace
	 */
	public void setPrintStackTrace( PrintStackTrace aPrintStackTrace ) {
		final Column<?> theColumn = LoggerField.toExceptionLoggerField( aPrintStackTrace ).getColumn();
		final int theIndex = indexOf( theColumn.getKey() );
		if ( theIndex != -1 ) {
			remove( theIndex );
			add( theIndex, theColumn );
		}
		else {
			add( theColumn );
		}
	}
}
