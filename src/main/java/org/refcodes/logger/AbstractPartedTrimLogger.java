// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;

import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.criteria.CriteriaUtility;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.Record;

/**
 * The {@link AbstractPartedTrimLogger} extends the
 * {@link AbstractPartedQueryLogger} with the functionality of a
 * {@link TrimLogger}. Delete operations with a query such as
 * {@link #deleteLogs(Criteria)} are applied to the partitions in the same
 * manner as done for {@link #findLogs(Criteria)}.
 *
 * @param <L> The type of the {@link TrimLogger} to be created.
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * @param <P> The type of the {@link Column}'s value used for partitioning the
 *        {@link TrimLogger}.
 */
abstract class AbstractPartedTrimLogger<L extends TrimLogger<T>, T, P extends T> extends AbstractPartedQueryLogger<L, T, P> implements TrimLogger<T> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AbstractPartedTrimLogger.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract parted trim logger.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 * 
	 * @see AbstractPartedQueryLogger#AbstractPartedQueryLogger(Column,
	 *      LoggerFactory, boolean)
	 */
	public AbstractPartedTrimLogger( Column<P> aPartitionColumn, LoggerFactory<L> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Instantiates a new abstract parted trim logger.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 * 
	 * @see AbstractPartedQueryLogger#AbstractPartedQueryLogger(Column, String,
	 *      LoggerFactory, boolean)
	 */
	public AbstractPartedTrimLogger( Column<P> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<L> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aDefaultLoggerName, aLoggerFactory, isPartitionAutoInitialize );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteLogs( Criteria aCriteria ) throws BadCriteriaException {
		final Set<P> thePartitions = CriteriaUtility.getPartitions( aCriteria, getPartitionColumn().getKey(), (Class<P>) getPartitionColumn().getType() );
		// ---------------------------------------------------------------------
		// Single partition to be addressed:
		// ---------------------------------------------------------------------
		if ( thePartitions.size() == 1 ) {
			final P ePartition = thePartitions.iterator().next();
			final L theLogger = getPartitionLogger( ePartition );
			if ( theLogger != null ) {
				aCriteria = CriteriaUtility.doRemovePartitionCriteria( aCriteria, getPartitionColumn().getKey(), thePartitions );
				theLogger.deleteLogs( aCriteria );
				return;
			}
			LOGGER.log( Level.WARNING, "No logger found for partition \"" + getPartitionUid( ePartition ) + "\": Now querying all partitions (fallback)!" );
		}
		else if ( thePartitions.size() == 0 && getFallbackLogger() != null ) {
			LOGGER.log( Level.FINE, "No partition found for provided criteria, now deleting logs from default logger!" );
			getFallbackLogger().deleteLogs( aCriteria );
			return;
		}

		// #####################################################################
		// In happy cases we should end the query here! Examine the logs in
		// order to determine on how to improve your partitioning (partitioning)
		// or your queries in order to always target at just one partition!
		// #####################################################################

		final Collection<L> theLoggers = getPartitionLoggers( thePartitions );

		// -------------
		// Do the query:
		// -------------
		LoggerUtility.deleteLogs( aCriteria, theLoggers );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		LoggerUtility.clearLogs( getLoggers() );
	}
}