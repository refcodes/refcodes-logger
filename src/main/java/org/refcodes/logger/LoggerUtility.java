// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.InvocationStrategy;
import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.exception.ExceptionAccessor;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordAccessor;
import org.refcodes.tabular.Records;
import org.refcodes.tabular.RecordsAccessor;
import org.refcodes.tabular.RecordsComposite;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;

/**
 * The Class LoggerUtility.
 */
public final class LoggerUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private LoggerUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Required to create a schema name for a {@link Logger} being part of a
	 * composite {@link Logger} (or its sub-types) and which is being created by
	 * a {@link LoggerFactory}.
	 * 
	 * @param aSchemaBody The body part of the schema name.
	 * @param aIndex The index number of the encapsulated {@link Logger} in
	 *        question.
	 * 
	 * @return The schema name created in a uniform way.
	 */
	public static String getSchemaName( String aSchemaBody, int aIndex ) {
		final String eSchemaName;
		eSchemaName = aSchemaBody + "." + new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( Integer.toString( aIndex ) ).withColumnWidth( 4 ).withFillChar( '0' ).toString();
		return eSchemaName;
	}

	/**
	 * Required to create a schema name for a {@link Logger} being part of a
	 * parted {@link Logger} (or its sub-types) and which is being created by a
	 * {@link LoggerFactory}.
	 * 
	 * @param aSchemaPrefix The prefix part of the schema name.
	 * @param aSchemaSuffix The suffix part of the schema name.
	 * 
	 * @return The schema name created in a uniform way.
	 */
	public static String getSchemaName( String aSchemaPrefix, String aSchemaSuffix ) {
		final String theSeparator = ( ( aSchemaPrefix != null && aSchemaPrefix.length() > 0 && aSchemaSuffix != null && aSchemaSuffix.length() > 0 ) ? "." : "" );
		final String theSchemaName = ( ( aSchemaPrefix != null ) ? ( aSchemaPrefix ) : ( "" ) ) + theSeparator + ( ( aSchemaSuffix != null ) ? ( aSchemaSuffix ) : ( "" ) );
		if ( theSchemaName.isEmpty() ) {
			throw new IllegalArgumentException( "The schema prefix \"" + aSchemaPrefix + "\" and the schema suffix + \"" + aSchemaSuffix + "\" cannot be used to create a valid schema name, created schema name is empty!" );
		}
		return theSchemaName;
	}

	/**
	 * Functionality to query multiple {@link QueryLogger} instances with the
	 * same query and returning a single composite {@link Records} instance
	 * containing all {@link Record} instances returned by the queried
	 * {@link QueryLogger} instances (multi-threaded). This functionality is in
	 * common for both composite as well as parted {@link QueryLogger} (or its
	 * sub-classes) instances.
	 * 
	 * @param <L> The {@link Logger} type to be used.
	 * @param <T> The type of the {@link Record} instances managed by the
	 *        {@link Logger}.
	 * @param aCriteria The query for querying the provides {@link QueryLogger}
	 *        (or sub-classes).
	 * @param aHeader The {@link Header} specifying the {@link Header} to be
	 *        contained in the result.
	 * @param aLimit The maximum number of elements to be returned by each
	 *        individual logger. Question: To avoid a quick and dirty solution
	 *        for this parameter to specify the number of elements in the
	 *        result, some strategy is to be applied to querying the
	 *        encapsulated {@link QueryLogger} instances, just dividing the
	 *        limit by the number of encapsulated {@link QueryLogger} instances
	 *        will often enough result in too few elements in the result. A
	 *        memory wasting approach would be to pass the limit to the
	 *        {@link RecordsComposite}, which stops serving elements when the
	 *        limit is reached :-(
	 * @param aLoggers The {@link QueryLogger} instances (or its sub-types)
	 *        which are to be queried.
	 * @param aRecordServeStrategy The strategy with which to serve the
	 *        {@link Record} instances in the returned {@link Records} instance,
	 *        meaning which encapsulated {@link QueryLogger}'s result is to be
	 *        served next upon the {@link Records#next()} invocation. The
	 *        {@link InvocationStrategy#ROUND_ROBIN} is proposed as of timeouts
	 *        and "forgotten" results in case one of the {@link QueryLogger}'s
	 *        result is fetched too late (this depends on how long the
	 *        underlying physical data sink provides for the result).
	 * 
	 * @return A {@link Records} instance containing all the found
	 *         {@link Record} instances.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <L extends QueryLogger<T>, T> Records<T> findLogs( final Criteria aCriteria, final Header<T> aHeader, int aLimit, Collection<L> aLoggers, InvocationStrategy aRecordServeStrategy ) throws BadCriteriaException {

		// ---------------------------------------------------------------------
		// Single partition to be addressed:
		// ---------------------------------------------------------------------
		if ( aLoggers.size() == 1 ) {
			final L theLogger = aLoggers.iterator().next();
			return theLogger.findLogs( aCriteria, aHeader, aLimit );
		}

		// ---------------------------------------------------------------------
		// Query more than one logger instance:
		// ---------------------------------------------------------------------
		final List<Thread> theThreads = new ArrayList<>();
		Thread eThread = null;
		for ( final L eLogger : aLoggers ) {
			eThread = new FindLogsDaemon<L, T>( eLogger, aCriteria, aHeader, aLimit );
			eThread.setPriority( Thread.NORM_PRIORITY );
			eThread.setDaemon( true );
			eThread.start();
			theThreads.add( eThread );
		}
		ControlFlowUtility.joinThreads( theThreads );
		final Throwable theException = toException( theThreads );
		if ( theException != null ) {
			throw new IllegalStateException( theException.getMessage(), theException );
		}
		final List<Records<?>> theRecords = getRecords( theThreads );
		return new RecordsComposite( aRecordServeStrategy, theRecords );
	}

	/**
	 * Functionality to query multiple {@link TrimLogger} instances with the
	 * same query to delete log lines from the provided {@link TrimLogger}
	 * instances (multi-threaded).
	 * 
	 * @param <L> The {@link Logger} type to be used.
	 * @param <T> The type of the {@link Record} instances managed by the
	 *        {@link Logger}.
	 * @param aCriteria The {@link Criteria} to be used when querying the
	 *        {@link TrimLogger} instances.
	 * @param aLoggers The {@link TrimLogger} instances (or its sub-types) which
	 *        are to be queried.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	public static <L extends TrimLogger<T>, T> void deleteLogs( Criteria aCriteria, Collection<L> aLoggers ) throws BadCriteriaException {

		// ---------------------------------------------------------------------
		// Single partition to be addressed:
		// ---------------------------------------------------------------------
		if ( aLoggers.size() == 1 ) {
			final L theLogger = aLoggers.iterator().next();
			theLogger.deleteLogs( aCriteria );
			return;
		}

		// ---------------------------------------------------------------------
		// Query more than one logger instances:
		// ---------------------------------------------------------------------
		final List<Thread> theThreads = new ArrayList<>();
		Thread eThread = null;
		for ( final L eLogger : aLoggers ) {
			eThread = new DeleteLogsDaemon<L, T>( eLogger, aCriteria );
			eThread.setPriority( Thread.NORM_PRIORITY );
			eThread.setDaemon( true );
			eThread.start();
			theThreads.add( eThread );
		}
		for ( Thread eJoinThread : theThreads ) {
			try {
				eJoinThread.join();
			}
			catch ( InterruptedException ignored ) {}
		}
		final Throwable theException = toException( theThreads );
		if ( theException != null ) {
			throw new IllegalStateException( theException.getMessage(), theException );
		}
	}

	/**
	 * Functionality to address multiple {@link TrimLogger} instances to clear
	 * the provided {@link TrimLogger} instances (multi-threaded).
	 * 
	 * @param <L> The {@link Logger} type to be used.
	 * @param <T> The type of the {@link Record} instances managed by the
	 *        {@link Logger}.
	 * @param aLoggers The {@link TrimLogger} instances (or its sub-types) which
	 *        are to be used.
	 */
	public static <L extends TrimLogger<T>, T> void clearLogs( Collection<L> aLoggers ) {
		final List<Thread> theThreads = new ArrayList<>();
		Thread eThread = null;
		for ( final L eLogger : aLoggers ) {
			eThread = new ClearLogsDaemon<L, T>( eLogger );
			eThread.setPriority( Thread.NORM_PRIORITY );
			eThread.setDaemon( true );
			eThread.start();
			theThreads.add( eThread );
		}
		for ( Thread eJoinThread : theThreads ) {
			try {
				eJoinThread.join();
			}
			catch ( InterruptedException ignored ) {}
		}
		final Throwable theException = toException( theThreads );
		if ( theException != null ) {
			throw new IllegalStateException( theException.getMessage(), theException );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gathers the records from the {@link RecordAccessor} instances found in
	 * the provided {@link List} and returns a {@link List} with those
	 * {@link Records} instances.
	 * 
	 * @param aRecordPropertyList the list with elements from which to gather
	 *        the {@link Records} instances in case an element implements the
	 *        {@link RecordAccessor} interface.
	 * 
	 * @return The {@link List} with the gathered {@link Records} instances.
	 */
	private static List<Records<?>> getRecords( List<?> aRecordPropertyList ) {
		final List<Records<?>> theRecords = new ArrayList<>();
		RecordsAccessor<?> eRecordsProperty = null;
		for ( Object eObj : aRecordPropertyList ) {
			if ( eObj instanceof RecordsAccessor ) {
				eRecordsProperty = (RecordsAccessor<?>) eObj;
				theRecords.add( eRecordsProperty.getRecords() );
			}
		}
		return theRecords;
	}

	/**
	 * Iterates through the objects passed in the collection and tests them
	 * whether them implement the {@link ExceptionAccessor}. The first one found
	 * implementing the {@link ExceptionAccessor} and returning an exception
	 * (instead of null), than this exception is returned.
	 * 
	 * @param aExceptionProperty The objects to test whether them implement the
	 *        {@link ExceptionAccessor}.
	 * 
	 * @return The first exception retrieved from the first object implementing
	 *         the {@link ExceptionAccessor} and not returning null when calling
	 *         {@link ExceptionAccessor#getException()}.
	 */
	private static Throwable toException( Collection<?> aExceptionProperty ) {
		ExceptionAccessor<?> eExceptionProperty = null;
		for ( Object eObj : aExceptionProperty ) {
			if ( eObj instanceof ExceptionAccessor ) {
				eExceptionProperty = (ExceptionAccessor<?>) eObj;
				if ( eExceptionProperty.getException() != null ) {
					return eExceptionProperty.getException();
				}
			}
		}
		return null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This daemon finds logs by the given criteria for the given
	 * {@link QueryLogger}.
	 *
	 * @param <L> the generic type
	 * @param <T> The type of the {@link Record} instances managed by the
	 *        {@link Logger}.
	 */
	private static class FindLogsDaemon<L extends QueryLogger<T>, T> extends Thread implements ExceptionAccessor<Exception>, LoggerAccessor<L>, RecordsAccessor<T> {

		private final L _logger;
		private Criteria _criteria = null;
		private Header<T> _columns = null;
		private Records<T> _records = null;
		private Exception _exception = null;
		private int _limit = -1;

		/**
		 * Constructs a {@link FindLogsDaemon} using the given
		 * {@link QueryLogger}.
		 *
		 * @param aLogger The {@link QueryLogger} for which this daemon is
		 *        responsible.
		 * @param aCriteria the criteria
		 * @param aHeader The {@link Header} to be used.
		 * @param aLimit Limit the result to the given number of rows.
		 */
		public FindLogsDaemon( L aLogger, Criteria aCriteria, Header<T> aHeader, int aLimit ) {
			_logger = aLogger;
			_criteria = aCriteria;
			_columns = aHeader;
			_limit = aLimit;
			setDaemon( true );
			setPriority( NORM_PRIORITY );
		}

		/**
		 * Gets the logger.
		 *
		 * @return the logger
		 */
		@Override
		public L getLogger() {
			return _logger;
		}

		/**
		 * Gets the records.
		 *
		 * @return the records
		 */
		@Override
		public Records<T> getRecords() {
			return _records;
		}

		/**
		 * Gets the exception.
		 *
		 * @return the exception
		 */
		@Override
		public Exception getException() {
			return _exception;
		}

		/**
		 * Run.
		 */
		@Override
		public void run() {
			try {
				_records = _logger.findLogs( _criteria, _columns, _limit );
			}
			catch ( Exception e ) {
				_exception = e;
			}
		}
	}

	/**
	 * This daemon deletes logs by the given criteria for the given
	 * {@link TrimLogger}.
	 *
	 * @param <L> the generic type
	 * @param <T> The type of the {@link Record} instances managed by the
	 *        {@link Logger}.
	 */
	private static class DeleteLogsDaemon<L extends TrimLogger<T>, T> extends Thread implements ExceptionAccessor<Exception>, LoggerAccessor<L> {

		private final L _logger;
		private Criteria _criteria = null;
		private Exception _exception = null;

		/**
		 * Constructs a {@link DeleteLogsDaemon} using the given
		 * {@link TrimLogger}.
		 * 
		 * @param aLogger The {@link TrimLogger} for which this daemon is
		 *        responsible.
		 * @param aCriteria The {@link Criteria} to be used.
		 */
		public DeleteLogsDaemon( L aLogger, Criteria aCriteria ) {
			_logger = aLogger;
			_criteria = aCriteria;
			setDaemon( true );
			setPriority( NORM_PRIORITY );
		}

		/**
		 * Gets the logger.
		 *
		 * @return the logger
		 */
		@Override
		public L getLogger() {
			return _logger;
		}

		/**
		 * Gets the exception.
		 *
		 * @return the exception
		 */
		@Override
		public Exception getException() {
			return _exception;
		}

		/**
		 * Run.
		 */
		@Override
		public void run() {
			try {
				_logger.deleteLogs( _criteria );
			}
			catch ( Exception e ) {
				_exception = e;
			}
		}
	}

	/**
	 * This daemon clears the given {@link TrimLogger}.
	 *
	 * @param <L> the generic type
	 * @param <T> The type of the {@link Record} instances managed by the
	 *        {@link Logger}.
	 */
	private static class ClearLogsDaemon<L extends TrimLogger<T>, T> extends Thread implements ExceptionAccessor<Exception>, LoggerAccessor<L> {

		private final L _logger;
		private Exception _exception = null;

		/**
		 * Constructs a {@link ClearLogsDaemon} using the given
		 * {@link TrimLogger}.
		 * 
		 * @param aLogger The {@link TrimLogger} for which this daemon is
		 *        responsible..
		 */
		public ClearLogsDaemon( L aLogger ) {
			_logger = aLogger;
			setDaemon( true );
			setPriority( NORM_PRIORITY );
		}

		/**
		 * Gets the logger.
		 *
		 * @return the logger
		 */
		@Override
		public L getLogger() {
			return _logger;
		}

		/**
		 * Gets the exception.
		 *
		 * @return the exception
		 */
		@Override
		public Exception getException() {
			return _exception;
		}

		/**
		 * Run.
		 */
		@Override
		public void run() {
			try {
				_logger.clear();
			}
			catch ( Exception e ) {
				_exception = e;
			}
		}
	}
}
