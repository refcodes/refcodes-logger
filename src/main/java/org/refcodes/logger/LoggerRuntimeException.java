// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.exception.AbstractRuntimeException;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordAccessor;

/**
 * The Class {@link LoggerRuntimeException}.
 */
public abstract class LoggerRuntimeException extends AbstractRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public LoggerRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerRuntimeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerRuntimeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerRuntimeException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * The Class {@link LoggerRecordRuntimeException}.
	 */
	@SuppressWarnings("rawtypes")
	protected abstract static class LoggerRecordRuntimeException extends LoggerRuntimeException implements RecordAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		protected Record<?> _record;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aRecord The record involved in this exception.
		 */
		public LoggerRecordRuntimeException( String aMessage, Record<?> aRecord, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_record = aRecord;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aRecord The record involved in this exception.
		 */
		public LoggerRecordRuntimeException( String aMessage, Record<?> aRecord, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_record = aRecord;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aRecord The record involved in this exception.
		 */
		public LoggerRecordRuntimeException( String aMessage, Record<?> aRecord, Throwable aCause ) {
			super( aMessage, aCause );
			_record = aRecord;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aRecord The record involved in this exception.
		 */
		public LoggerRecordRuntimeException( String aMessage, Record<?> aRecord ) {
			super( aMessage );
			_record = aRecord;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aRecord The record involved in this exception.
		 */
		public LoggerRecordRuntimeException( Record<?> aRecord, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_record = aRecord;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aRecord The record involved in this exception.
		 */
		public LoggerRecordRuntimeException( Record<?> aRecord, Throwable aCause ) {
			super( aCause );
			_record = aRecord;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Record<?> getRecord() {
			return _record;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _record };
		}
	}
}
