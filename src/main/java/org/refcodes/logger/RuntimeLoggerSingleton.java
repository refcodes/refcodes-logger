// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.data.Folders;
import org.refcodes.runtime.SystemProperty;

/**
 * This {@link RuntimeLoggerSingleton} provides a {@link RuntimeLogger}
 * singleton configured by a "<code>runtimelogger-config.xml</code>" file find
 * in one of the several locations relative to your application's base
 * directory: The applications base directory (where your JAR or your classes
 * reside) is taken and a list of directories (as defined in the
 * {@link Folders#CONFIG_DIRS}) relative to this base directory is generated :
 * The actual directories being looked at (in case them exist) are as follows,
 * relative to your applications base directory:
 * <ul>
 * <li>../config"</li>
 * <li>../etc"</li>
 * <li>../settings"</li>
 * <li>../.config"</li>
 * <li>../settings"</li>
 * <li>."</li>
 * <li>./config"</li>
 * <li>./etc"</li>
 * <li>./settings"</li>
 * <li>./.config"</li>
 * <li>./settings"</li>
 * </ul>
 * In case you pass a JVM argument via "-Dconfig.dir=path_to_your_config_dir"
 * (where path_to_your_config_dir stands for the path to the directory where you
 * placed configuration files such as the
 * "<code>runtimelogger-config.xml</code>"" file), then your
 * path_to_your_config_dir is placed first in the list of configuration
 * directories to look at (in case the directory exists).See
 * {@link SystemProperty#CONFIG_DIR}
 */
public class RuntimeLoggerSingleton implements RuntimeLogger {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static RuntimeLogger _runtimeLoggerSingleton;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new runtime logger singleton.
	 *
	 * @param aRuntimeLogger the runtime logger
	 */
	protected RuntimeLoggerSingleton( RuntimeLogger aRuntimeLogger ) {}

	// /////////////////////////////////////////////////////////////////////////
	// SINGLETON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link RuntimeLoggerSingleton}.
	 * 
	 * @return The {@link RuntimeLogger} singleton's instance.
	 * 
	 * @throws LoggerInstantiationRuntimeException Thrown in case instantiating
	 *         a {@link Logger} ({@link RuntimeLogger}) failed
	 */
	public static RuntimeLogger getInstance() {
		if ( _runtimeLoggerSingleton == null ) {
			synchronized ( RuntimeLoggerSingleton.class ) {
				if ( _runtimeLoggerSingleton == null ) {
					_runtimeLoggerSingleton = RuntimeLoggerFactorySingleton.fromConfigurationFile( RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH, RuntimeLogger.RUNTIME_LOGGER_CONFIG );
				}
			}
		}
		return _runtimeLoggerSingleton;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _runtimeLoggerSingleton.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LogPriority getLogPriority() {
		return _runtimeLoggerSingleton.getLogPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aMarker, String aMessage ) {
		_runtimeLoggerSingleton.log( aMarker, aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.log( aPriority, aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aMarker, String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.log( aMarker, aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.log( aPriority, aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLog( LogPriority aPriority ) {
		return _runtimeLoggerSingleton.isLog( aPriority );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String aMessage ) {
		_runtimeLoggerSingleton.trace( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.trace( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogTrace() {
		return _runtimeLoggerSingleton.isLogTrace();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String aMessage ) {
		_runtimeLoggerSingleton.debug( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.debug( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogDebug() {
		return _runtimeLoggerSingleton.isLogDebug();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String aMessage ) {
		_runtimeLoggerSingleton.info( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.info( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogInfo() {
		return _runtimeLoggerSingleton.isLogInfo();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notice( String aMessage ) {
		_runtimeLoggerSingleton.notice( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notice( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.notice( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogNotice() {
		return _runtimeLoggerSingleton.isLogNotice();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage ) {
		_runtimeLoggerSingleton.warn( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.warn( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.warn( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.warn( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogWarn() {
		return _runtimeLoggerSingleton.isLogWarn();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage ) {
		_runtimeLoggerSingleton.error( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.error( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.error( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.error( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogError() {
		return _runtimeLoggerSingleton.isLogError();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage ) {
		_runtimeLoggerSingleton.critical( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.critical( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.critical( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.critical( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogCritical() {
		return _runtimeLoggerSingleton.isLogCritical();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage ) {
		_runtimeLoggerSingleton.alert( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.alert( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.alert( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.alert( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogAlert() {
		return _runtimeLoggerSingleton.isLogAlert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage ) {
		_runtimeLoggerSingleton.panic( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.panic( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.panic( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.panic( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogPanic() {
		return _runtimeLoggerSingleton.isLogPanic();
	}
}
