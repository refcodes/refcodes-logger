// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.refcodes.data.Field;
import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.TypeAccessor;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.ColumnAccessor;
import org.refcodes.tabular.DateColumn;
import org.refcodes.tabular.ExceptionColumn;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.IntColumn;
import org.refcodes.tabular.PrintStackTrace;
import org.refcodes.tabular.StringColumn;

/**
 * Defines default logger {@link Header}.
 */
@SuppressWarnings("rawtypes")
public enum LoggerField implements ColumnAccessor, KeyAccessor<String>, TypeAccessor {

	// @formatter:off
	LOG_LINE_NUMBER(new IntColumn( Field.LOG_LINE_NUMBER.getName() )),
	
	LOG_PRIORITY(new LogPriorityColumn( Field.LOG_PRIORITY.getName() )), 
	
	LOG_DATE(new DateColumn( Field.LOG_DATE.getName() )),
	
	LOG_THREAD_NAME(new StringColumn( Field.LOG_THREAD_NAME.getName() )),
	
	LOG_SESSION_ID(new StringColumn( Field.LOG_SESSION_ID.getName() )),
	
	LOG_REQUEST_ID(new StringColumn( Field.LOG_REQUEST_ID.getName() )),
	
	LOG_MESSAGE(new StringColumn( Field.LOG_MESSAGE.getName() )),
	
	LOG_NONE_STACKTRRACE_EXCEPTION(new ExceptionColumn( Field.LOG_EXCEPTION.getName(), PrintStackTrace.NONE )),
	
	LOG_COMPACT_STACKTRACE_EXCEPTION(new ExceptionColumn( Field.LOG_EXCEPTION.getName(), PrintStackTrace.COMPACT)),
	
	LOG_EXPLODED_STACKTRACE_EXCEPTION(new ExceptionColumn( Field.LOG_EXCEPTION.getName(), PrintStackTrace.EXPLODED)),
	
	LOG_FULLY_QUALIFIED_CLASS_NAME(new StringColumn( Field.LOG_FULLY_QUALIFIED_CLASS_NAME.getName() )),
	
	LOG_METHODE_NAME(new MethodNameColumnImpl( Field.LOG_METHOD_NAME.getName() )), 
	
	LOG_CLASS_LINE_NUMBER(new IntColumn( Field.LOG_CLASS_LINE_NUMBER.getName() ));
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Column<?> _column;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new logger field.
	 *
	 * @param aColumn the column
	 */
	private LoggerField( Column<?> aColumn ) {
		_column = aColumn;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Column<?> getColumn() {
		return _column;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _column.getKey();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> getType() {
		return _column.getType();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To exception logger field.
	 *
	 * @param aPrintStackTrace the print stack trace
	 * 
	 * @return the logger field
	 */
	public static LoggerField toExceptionLoggerField( PrintStackTrace aPrintStackTrace ) {
		switch ( aPrintStackTrace ) {
		case COMPACT:
			return LOG_COMPACT_STACKTRACE_EXCEPTION;
		case NONE:
			return LOG_NONE_STACKTRRACE_EXCEPTION;
		case EXPLODED:
		default:
			return LOG_EXPLODED_STACKTRACE_EXCEPTION;
		}
	}

	/**
	 * Gets a predefined column by key (name).
	 * 
	 * @param aKey The key for which to get the column.
	 * 
	 * @return The column in question or <code>null</code> if none was found.
	 */
	public static Column<?> fromKey( String aKey ) {
		final List<LoggerField> theList = Arrays.asList( LoggerField.values() );
		final Iterator<LoggerField> e = theList.iterator();
		LoggerField eDefinition = null;
		while ( e.hasNext() ) {
			eDefinition = e.next();
			if ( eDefinition.getColumn().getKey().equalsIgnoreCase( aKey ) ) {
				return eDefinition.getColumn();
			}
		}
		return null;
	}
}
