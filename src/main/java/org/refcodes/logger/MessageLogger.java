// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.text.MessageFormat;

/**
 * The aMessage logger is the generic version of the {@link RuntimeLogger}.
 * Instead of being bound to a {@link LogPriority} to mark your log lines, you
 * may use any type as marker.
 *
 * @param <M> The marker used to mark the aMessage to be logged.
 */
public interface MessageLogger<M> {

	/**
	 * Creates a new log aMessage.
	 * 
	 * @param aMarker The marker for the aMessage.
	 * @param aMessage The aMessage to be logged.
	 */
	void log( M aMarker, String aMessage );

	/**
	 * Creates a new log aMessage. The placeholders in the aMessage
	 * {@link String} are being replaced by the provided arguments.
	 * Implementations of this interface are recommended to use Java's
	 * {@link MessageFormat}'s syntax, the placeholder being replaced by the
	 * first argument is identified by "{0}" (without the quotes), the second
	 * one by "{1}", the third one by "{3}" and so on: Given the aMessage to be
	 * "{0} + {1} = {2}" and the three argument be x, y and x + y, then "{0}" is
	 * replaced by the value of x, "{1}" is replaced by the value of y and "{2}"
	 * is replaced by the value of x + y.
	 * 
	 * @param aMarker The marker for the aMessage.
	 * @param aMessage The aMessage to be logged.
	 * @param aArguments The arguments used when replacing the placeholders.
	 */
	void log( M aMarker, String aMessage, Object... aArguments );

}
