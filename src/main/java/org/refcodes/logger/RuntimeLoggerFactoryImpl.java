// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.refcodes.data.Delimiter;
import org.refcodes.mixin.NameAccessor.NameMutator;
import org.refcodes.properties.Properties;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;

/**
 * You configure your {@link RuntimeLoggerFactoryImpl} by providing a
 * "<code>runtimelogger.ini</code>" file (see
 * "http://www.refcodes.org/refcodes/refcodes-logger") in one of those locations
 * relative to your main class's location:
 * <ul>
 * <li>.</li>
 * <li>./config</li>
 * <li>./etc</li>
 * <li>./settings</li>
 * <li>./.config</li>
 * <li>./.settings</li>
 * <li>../config</li>
 * <li>../etc</li>
 * <li>../settings</li>
 * <li>../.config</li>
 * <li>../.settings</li>
 * </ul>
 * Given your main class's JAR file resides in the folder /opt/app/lib, then the
 * valid locations for the "<code>runtimelogger.ini</code>" file are:
 * <ul>
 * <li>/opt/app/lib</li>
 * <li>/opt/app/lib/config</li>
 * <li>/opt/app/lib/etc</li>
 * <li>/opt/app/lib/settings</li>
 * <li>/opt/app/lib/.config</li>
 * <li>/opt/app/lib/.settings</li>
 * <li>/opt/app/config</li>
 * <li>/opt/app/etc</li>
 * <li>/opt/app/settings</li>
 * <li>/opt/app/.config</li>
 * <li>/opt/app/.settings</li>
 * </ul>
 * In case you pass a JVM argument via "-Dconfig.dir=path_to_your_config_dir"
 * (where path_to_your_config_dir stands for the path to the directory where you
 * placed configuration files such as the "<code>runtimelogger.ini</code>"
 * file), then your path_to_your_config_dir is placed first in the list of
 * configuration directories to look at (in case the directory exists).See
 * {@link SystemProperty#CONFIG_DIR} and {@link ConfigLocator#getFolders()}. The
 * "<code>runtimelogger.ini</code>" configuration is deadly simple:
 * 
 * <pre> [root]
 * 
 * runtimelogger=org.refcodes.logger.RuntimeLoggerImpl
 * runtimelogger/logPriority=INFO
 * runtimelogger/logger=org.refcodes.logger.SystemLogger
 * 
 * [com.acme]
 * 
 * runtimelogger=org.refcodes.logger.RuntimeLoggerImpl
 * runtimelogger/logPriority=INFO runtimelogger/name=com.acme
 * runtimelogger/logger=org.refcodes.logger.SystemLogger </pre>
 * 
 * The XML element nesting represents the Java package for which the therein
 * configured {@link RuntimeLogger} is responsible; e.g. a log issued from
 * inside a class located in the package "com.acme" (or in one of its
 * sub-packages) will be handled by the "AcmeLogger". In case no logger is found
 * for a given package, then the root logger found in the &lt;root&gt; ...
 * &lt;/root&gt; element is used. Useful to know that a logger for a package
 * namespace is only created once.
 * <p>
 * If you like logs colored nicely with ANSI Escape-Codes, then you will love
 * the ConsoleLoggerSingleton:
 * 
 * <pre> {@code
 * <?xml version="1.0" encoding="ISO-8859-1" ?>
 * <config>
 * 	<root config-class=
"org.refcodes.logger.RuntimeLoggerImpl" logPriorityName="INFO" name="*">
 * 		<logger config-class=
"org.refcodes.logger.alt.cli.ConsoleLoggerSingleton" />
 * 	</root>
 * </config>
 * } </pre>
 * 
 * Make sure to include the "refcodces-logger-alt-console" dependency in your
 * build setup to include the "ConsoleLoggerSingleton" logger.
 */
public class RuntimeLoggerFactoryImpl implements RuntimeLoggerFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME_PROPERTY = "name";
	private static final String CONFIG_LOCATOR = "runtimelogger";
	private static final RuntimeLoggerImpl FALLBACK_RUNTIME_LOGGER = new RuntimeLoggerImpl( new SystemLogger() );
	private static final String SKIP_LOGGER_CONFIG_FILE_LIB = "org.apache.commons.configuration.DefaultFileSystem";
	private static final Map<String, Properties> CONFIG_TO_PROPERTIES = new WeakHashMap<>();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ConcurrentHashMap<String, RuntimeLogger> _nameToRuntimeLoggerMap = new ConcurrentHashMap<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	protected RuntimeLoggerFactoryImpl() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RuntimeLogger create() {
		// ---------------------------------------------------------------------
		// ATTENTION: As the StackTraceElement caller array is evaluated, we
		// must not delegate this call to one "master" implementation as this
		// will falsify the actual origin of this factory call.
		// ---------------------------------------------------------------------
		final String className = Execution.getCallerStackTraceElement( RuntimeLoggerFactoryImpl.class.getPackage().getName() ).getClassName();
		return create( className, (Map<String, String>) null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RuntimeLogger create( Map<String, String> aProperties ) {
		// ---------------------------------------------------------------------
		// ATTENTION: As the StackTraceElement caller array is evaluated, we
		// must not delegate this call to one "master" implementation as this
		// will falsify the actual origin of this factory call.
		// ---------------------------------------------------------------------
		final String className = Execution.getCallerStackTraceElement( RuntimeLoggerFactoryImpl.class ).getClassName();
		return create( className, aProperties );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RuntimeLogger create( String aIdentifier ) {
		return create( aIdentifier, null );
	}

	/**
	 * {@inheritDoc} The provided identifier represents a level hierarchy as
	 * defined in the {@link RuntimeLogger#RUNTIME_LOGGER_CONFIG} XML file. The
	 * level hierarchy is actually the path to the XML element providing the
	 * {@link RuntimeLogger}'s configuration. Each level is separated by the
	 * succeeding level with a dot "." as defined by the
	 * {@link Delimiter#NAMESPACE} char value. The root logger's level hierarchy
	 * (XML path) is defined by the
	 * {@link RuntimeLogger#ROOT_LOGGER_ELEMENT_PATH}; at the time of this
	 * writing it was set to "org.refcodes.logger.runtimeLogger". The properties
	 * are currently ignored.
	 */
	@Override
	public RuntimeLogger create( String aIdentifier, Map<String, String> aProperties ) {
		if ( aIdentifier == null ) {
			throw new IllegalArgumentException( "The identifier must not be null; please provide a valid identifier." );
		}
		RuntimeLogger theRuntimeLogger = _nameToRuntimeLoggerMap.get( aIdentifier );
		if ( theRuntimeLogger != null ) {
			return theRuntimeLogger;
		}
		final List<String> theNames = new ArrayList<>();

		// Initialize "root" logger first ("master configuration") |-->
		RuntimeLogger theRootLogger = _nameToRuntimeLoggerMap.get( RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH );
		if ( theRootLogger == null ) {
			try {
				theRootLogger = fromConfigurationFile( RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH, RuntimeLogger.RUNTIME_LOGGER_CONFIG );
			}
			catch ( LoggerInstantiationRuntimeException e ) {
				final Logger<Object> theLogger = new SystemLogger();
				theRootLogger = new RuntimeLoggerImpl( theLogger );
				theRootLogger.warn( "Using fallback logger as the <" + RuntimeLogger.RUNTIME_LOGGER_CONFIG + ".*> file does neither contain a valid runtime logger configuration for the namespace <" + aIdentifier + "> (or its children) nor for the root logger: " + e.getMessage() );
			}
			if ( theRootLogger == null ) {
				throw new LoggerInstantiationRuntimeException( "The <{0}(.*)> file does neither contain a valid runtime logger configuration for the namespace <{1}> (or its children) nor for the root logger!", RuntimeLogger.RUNTIME_LOGGER_CONFIG, aIdentifier );
			}
			_nameToRuntimeLoggerMap.putIfAbsent( RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH, theRootLogger );
		}

		String eIdentifier = aIdentifier.replace( Delimiter.INNER_CLASS.getChar(), Delimiter.PACKAGE_HIERARCHY.getChar() );
		int i = aIdentifier.length();
		while ( i > 0 ) {
			eIdentifier = eIdentifier.substring( 0, i );
			theNames.add( eIdentifier );
			theRuntimeLogger = _nameToRuntimeLoggerMap.get( eIdentifier );
			if ( theRuntimeLogger == null ) {
				try {
					theRuntimeLogger = fromConfigurationFile( eIdentifier, RuntimeLogger.RUNTIME_LOGGER_CONFIG );
					break;
				}
				catch ( LoggerInstantiationRuntimeException ignore ) {}
			}
			else {
				break;
			}
			i = eIdentifier.lastIndexOf( Delimiter.NAMESPACE.getChar() );
		}
		if ( theRuntimeLogger == null ) {

			// -----------------------------------------------------------------
			// Skip creation for the logger configuration library as it requests
			// a logger during logger configuration time, where the logger
			// configuration has not been evaluated by the very same library:
			// -----------------------------------------------------------------
			if ( SKIP_LOGGER_CONFIG_FILE_LIB.equals( aIdentifier ) ) {
				return FALLBACK_RUNTIME_LOGGER;
			}
			// -----------------------------------------------------------------

			if ( !RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH.equals( aIdentifier ) ) {
				theRuntimeLogger = theRootLogger;
			}
		}
		for ( String eName : theNames ) {
			_nameToRuntimeLoggerMap.putIfAbsent( eName, theRuntimeLogger );
		}
		final RuntimeLogger theRaceWinner = _nameToRuntimeLoggerMap.get( aIdentifier );
		return theRaceWinner != null ? theRaceWinner : theRuntimeLogger;

	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Loads the configured {@link RuntimeLogger} from the given configuration
	 * file. Various locations as well file endings are probed as of the
	 * {@link ApplicationProperties} as well as of the {@link ConfigLocator#ALL}
	 * definition.
	 * 
	 * @param aNamespace The path (package) for which to create the logger.
	 * @param aConfigFileName The (base) file name of the configuration file.
	 * 
	 * @return The according logger.
	 * 
	 * @throws LoggerInstantiationRuntimeException Thrown in case instantiating
	 *         a {@link Logger} ({@link RuntimeLogger}) failed,
	 */
	public static RuntimeLogger fromConfigurationFile( String aNamespace, String aConfigFileName ) {
		try {
			Properties theProperties = CONFIG_TO_PROPERTIES.get( aConfigFileName );
			if ( theProperties == null ) {
				theProperties = new ApplicationProperties().withResourceClass( aConfigFileName );
				CONFIG_TO_PROPERTIES.put( aConfigFileName, theProperties );
			}
			final String thePath = aNamespace.replace( Delimiter.PACKAGE_HIERARCHY.getChar(), Delimiter.PATH.getChar() );

			// Fill up with ROOT logger's defaults |-->
			final Properties theRootProperties = theProperties.retrieveFrom( RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH );
			if ( RuntimeLogger.ROOT_LOGGER_ELEMENT_PATH.equals( aNamespace ) ) {
				theProperties = theRootProperties;
			}
			else {
				final PropertiesBuilder theNewProperties = new PropertiesBuilderImpl( theProperties.retrieveFrom( thePath ) );
				String[] ePathElements;
				final Iterator<String> e = theNewProperties.keySet().iterator();
				while ( e.hasNext() ) {
					ePathElements = theNewProperties.toPathElements( e.next() );
					if ( ePathElements == null || ePathElements.length == 0 || !CONFIG_LOCATOR.equals( ePathElements[0] ) ) {
						e.remove();
					}
				}
				if ( !theNewProperties.isEmpty() ) {
					for ( String eKey : theRootProperties.keySet() ) {
						if ( !theNewProperties.containsKey( eKey ) ) {
							theNewProperties.put( eKey, theRootProperties.get( eKey ) );
						}
					}
				}
				theProperties = theNewProperties;
			}
			// Fill up with ROOT logger's defaults <--|

			final RuntimeLogger theRuntimeLogger = theProperties.toType( CONFIG_LOCATOR, RuntimeLogger.class );
			if ( theRuntimeLogger instanceof NameMutator ) {
				final String theName = theProperties.get( CONFIG_LOCATOR, NAME_PROPERTY );
				if ( theName == null || theName.isEmpty() ) {
					( (NameMutator) theRuntimeLogger ).setName( aNamespace );
				}
				else {
					( (NameMutator) theRuntimeLogger ).setName( theName );
				}
			}
			return theRuntimeLogger;
		}
		catch ( Exception e ) {
			throw new LoggerInstantiationRuntimeException( "Cannot create logger from configuration file <{0}> with namespace (for package) <{1}>!", aConfigFileName, aNamespace, e );
		}
	}
}
