// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

/**
 * Provides an accessor for a column layout property.
 */
public interface ColumnLayoutAccessor {

	/**
	 * Retrieves the column layout from the column layout property.
	 * 
	 * @return The column layout stored by the column layout property.
	 */
	ColumnLayout getColumnLayout();

	/**
	 * Provides a mutator for a column layout property.
	 */
	public interface ColumnLayoutMutator {

		/**
		 * Sets the column layout for the column layout property.
		 * 
		 * @param aColumnLayout The column layout to be stored by the column
		 *        layout property.
		 */
		void setColumnLayout( ColumnLayout aColumnLayout );
	}

	/**
	 * Provides a builder method for a column layout property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ColumnLayoutBuilder<B extends ColumnLayoutBuilder<B>> {

		/**
		 * Sets the column layout for the column layout property.
		 * 
		 * @param aColumnLayout The column layout to be stored by the column
		 *        layout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withColumnLayout( ColumnLayout aColumnLayout );
	}

	/**
	 * Provides a column layout property.
	 */
	public interface ColumnLayoutProperty extends ColumnLayoutAccessor, ColumnLayoutMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ColumnLayout}
		 * (setter) as of {@link #setColumnLayout(ColumnLayout)} and returns the
		 * very same value (getter).
		 * 
		 * @param aColumnLayout The {@link ColumnLayout} to set (via
		 *        {@link #setColumnLayout(ColumnLayout)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ColumnLayout letColumnLayout( ColumnLayout aColumnLayout ) {
			setColumnLayout( aColumnLayout );
			return aColumnLayout;
		}
	}
}
