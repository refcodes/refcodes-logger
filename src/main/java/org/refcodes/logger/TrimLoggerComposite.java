// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.io.IOException;

import org.refcodes.component.ComponentComposite;
import org.refcodes.component.ComponentUtility;
import org.refcodes.component.InitializeException;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.tabular.Record;

/**
 * The {@link TrimLoggerComposite} is a ready to use implementation of a
 * composite {@link TrimLogger} extending the
 * {@link AbstractTrimLoggerComposite}.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * 
 * @see AbstractTrimLoggerComposite
 */
public class TrimLoggerComposite<T> extends AbstractTrimLoggerComposite<TrimLogger<T>, T> implements ComponentComposite {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutionStrategy _componentExecutionStrategy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link TrimLoggerComposite} with the according
	 * parameters.
	 *
	 * @param aLoggers the {@link TrimLogger} instances populating this
	 *        composite.
	 * 
	 * @see AbstractTrimLoggerComposite#AbstractTrimLoggerComposite(TrimLogger...)
	 */
	@SafeVarargs
	public TrimLoggerComposite( TrimLogger<T>... aLoggers ) {
		this( ExecutionStrategy.JOIN, aLoggers );
	}

	/**
	 * Similar to the {@link #TrimLoggerComposite(TrimLogger...)} with the
	 * additional option of determining the execution strategy of the state
	 * change request calls for the encapsulated {@link TrimLogger} instances
	 * (as of {@link ComponentComposite}).
	 *
	 * @param aComponentExecutionStrategy The strategy to be used when invoking
	 *        a component's (encapsulated {@link TrimLogger} instance's) state
	 *        change request call (as of {@link ComponentComposite}).
	 * @param aLoggers the {@link TrimLogger} instances populating this
	 *        composite.
	 */
	@SafeVarargs
	public TrimLoggerComposite( ExecutionStrategy aComponentExecutionStrategy, TrimLogger<T>... aLoggers ) {
		super( aLoggers );
		_componentExecutionStrategy = aComponentExecutionStrategy;
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMPONENT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		ComponentUtility.initialize( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		ComponentUtility.start( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		ComponentUtility.pause( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		ComponentUtility.resume( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		ComponentUtility.stop( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decompose() {
		ComponentUtility.decompose( _componentExecutionStrategy, getLoggers() );
		getLoggers().clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		ComponentUtility.flush( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		super.destroy();
		ComponentUtility.destroy( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		ComponentUtility.reset( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		ComponentUtility.open( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		ComponentUtility.close( _componentExecutionStrategy, getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ComponentUtility.dispose( _componentExecutionStrategy, getLoggers() );
	}
}
