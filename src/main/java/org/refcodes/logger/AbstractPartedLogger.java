// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.refcodes.component.ComponentUtility;
import org.refcodes.component.Decomposable;
import org.refcodes.component.InitializeException;
import org.refcodes.criteria.Criteria;
import org.refcodes.mixin.UniversalIdAccessor;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.TabularUtility;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * The {@link org.refcodes.logger.PartedLogger} is a partitioning logger which
 * encapsulates {@link org.refcodes.logger.Logger} instances or
 * {@link org.refcodes.logger.LoggerComposite} instances (or sub-classes of it)
 * representing partitions.
 * <p>
 * This means: A partition is regarded to be a dedicated physical data sink or a
 * {@link org.refcodes.logger.LoggerComposite} containing
 * {@link org.refcodes.logger.Logger} instances attached to physical data sinks.
 * A physical data sink may be a database (SQL or NoSQL), a files-system or
 * volatile memory (in memory). To be more concrete: A physical data sink may be
 * a domain when using Amazon's SimpleDB, it may be a database table when using
 * MySQL, it may be a HashMap or a List when using in memory storage.
 * <p>
 * The {@link org.refcodes.tabular.Record} instances as managed by the
 * {@link org.refcodes.logger.Logger} instances are mapped to the fields of the
 * physical data sink (e.g. table columns regarding databases).
 * <p>
 * The {@link org.refcodes.tabular.Record} instances are stored to, retrieved
 * from or deleted from dedicated partitions depending on partitioning
 * {@link org.refcodes.criteria.Criteria} contained in the
 * {@link org.refcodes.tabular.Record} instances (or the query
 * {@link org.refcodes.criteria.Criteria} instances). The
 * {@link org.refcodes.criteria.Criteria} (e.g. the column partition
 * {@link org.refcodes.criteria.Criteria} in a
 * {@link org.refcodes.tabular.Record}) as provided to the #log(Record) method
 * is used by the {@link org.refcodes.logger.PartedLogger} to select the
 * partition to be addressed. In case of query operations the query
 * {@link org.refcodes.criteria.Criteria} is used to determine the targeted
 * partition.
 * <p>
 * (in case no partition can be determined and a fallback logger has been
 * configured, then data may get logged to the fallback logger)
 * <p>
 * In practice there can be several (composite) logger instances being the
 * partitions of the {@link org.refcodes.logger.PartedLogger}, each individually
 * addressed by the partitioning {@link org.refcodes.criteria.Criteria}.
 * <p>
 * This approach a) helps us scale horizontally per partition when using
 * {@link org.refcodes.logger.LoggerComposite} instances per partition and b)
 * helps limiting the traffic on those horizontally scaling (composite) logger
 * instances by partitioning the data per {@link org.refcodes.criteria.Criteria}
 * using the parted logger (or its sub-classes): Partitioning simply means
 * switching to the partition defined by the
 * {@link org.refcodes.criteria.Criteria} to perform the according logger
 * operation.
 * <p>
 * Not having the {@link org.refcodes.logger.PartedLogger} (or a sub-class of
 * it) would aCause all the traffic for all
 * {@link org.refcodes.criteria.Criteria} to hit just a single (composite)
 * {@link org.refcodes.logger.Logger}, limiting the possibility to scale
 * endlessly (this one logger would be the bottleneck, even when being massively
 * scaled horizontally): In particular this applies when looking at the extended
 * versions of the {@link org.refcodes.logger.PartedLogger} such as the
 * {@link org.refcodes.logger.PartedQueryLogger} and the
 * {@link org.refcodes.logger.PartedTrimLogger} where query requests are passed
 * only to the partition which contains the required data: Increasing query
 * traffic is parted and does not hit increasingly a single (composite) logger.
 * <p>
 * A {@link org.refcodes.tabular.Record} to be assigned to a partition must
 * provide a column, the so called partition column, whose value is used to
 * determine which partition is to be addressed. The partition identifying
 * column is passed upon construction to this
 * {@link org.refcodes.logger.PartedLogger}. Specializations may hide this
 * parameter from their constructors and pass their partitioning column from
 * inside their constructor to the super constructor.
 * <p>
 * The {@link org.refcodes.logger.PartedQueryLogger} extends the
 * {@link org.refcodes.logger.PartedLogger} with the functionality of a query
 * logger. Any query operations, such as #findLogs(Criteria), are targeted at
 * that partition containing the queried data. For this to work, the query must
 * obey some rules:
 * <p>
 * The query is to contain an {@link org.refcodes.criteria.EqualWithCriteria}
 * addressing the partition in an unambiguous way; by being part of a root level
 * {@link org.refcodes.criteria.AndCriteria} or an unambiguously nested
 * {@link org.refcodes.criteria.AndCriteria} hierarchy. More than one partition
 * gets detected when unambiguous {@link org.refcodes.criteria.OrCriteria} are
 * applied to the partition criteria. In such cases, the query is addressed to
 * the potential partitions.
 * <p>
 * In case it was not possible to identify any partitions, then as a fallback,
 * all partitions are queried.
 * <p>
 * Query results are taken from from the invoked partitions (in normal cases
 * this would be a single partition) round robin. the first result is taken from
 * the first queried partition's result set (
 * {@link org.refcodes.tabular.Record} s), the next result from the next queried
 * partition and so on to start over again with the first queried partition.
 * Round robin has been used to prevent invalidation of physical data sinks's
 * result sets as of timeouts.
 * <p>
 * The {@link org.refcodes.logger.PartedTrimLogger} extends the parted query
 * logger with the functionality of a trim logger. Delete operations with a
 * query such as #deleteLogs(Criteria) are applied to the partitions in the same
 * manner as done for #findLogs(Criteria).
 *
 * @param <L> The type of the {@link Logger} to be created.
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * @param <P> The type of the {@link Column}'s value used for partitioning the
 *        {@link Logger}.
 * 
 * @see PartedLogger
 */
abstract class AbstractPartedLogger<L extends Logger<T>, T, P extends T> implements Logger<T> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AbstractPartedLogger.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Column<P> _partitionColumn;
	private boolean _isPartitionAutoInitialize;
	private LoggerFactory<L> _loggerFactory;
	private final Map<P, L> _partitionToLoggerMap = new HashMap<>();
	private L _fallBackLogger;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractPartedLogger}, a partitioning
	 * {@link Logger}. A {@link LoggerFactory} is used to create the (composite)
	 * {@link Logger} instances representing the dedicated partitions. This is
	 * achieved by the method {@link #initPartition(UniversalIdStorage)}. The
	 * type of the {@link Logger} instance being created depends on the provided
	 * {@link LoggerFactory}. We may provide a {@link LoggerFactory} producing
	 * plain simple {@link Logger} instances or more complex
	 * {@link LoggerComposite} instances or even {@link PartedLogger} instances.
	 * Partitions may get automatically created when enabling the partition auto
	 * initialize option. When logging a {@link Record} (via the method
	 * {@link #log(Record)}) whose partitioning {@link Column} addresses a non
	 * existing partition, then the {@link LoggerFactory} us used to create that
	 * partition on the fly. This option does not regard the find or delete
	 * methods. In case no partition criteria was provided or determined, then a
	 * fallback {@link Logger} may be used for logging such data to. The
	 * provided {@link Column} specifies a {@link Record}'s {@link Column}
	 * holding the value being used to choose the partition from. Extensions of
	 * the {@link AbstractPartedLogger} use this partition {@link Column} to
	 * determine an addressed partition from a query {@link Criteria}.
	 * 
	 * @param aPartitionColumn Defines the {@link Column} identifying a
	 *        partition.
	 * @param aLoggerFactory The {@link LoggerFactory} to be used when creating
	 *        partitions.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public AbstractPartedLogger( Column<P> aPartitionColumn, LoggerFactory<L> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		this( aPartitionColumn, null, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Same as
	 * {@link #AbstractPartedLogger(Column, LoggerToRuntimeLoggerFactory, boolean)}
	 * with the difference that a fallback {@link Logger} is being supported
	 * when no partition can be determined for an operation.
	 *
	 * @param aPartitionColumn Defines the {@link Column} identifying a
	 *        partition.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory The {@link LoggerFactory} to be used when creating
	 *        partitions.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 */
	public AbstractPartedLogger( Column<P> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<L> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		_loggerFactory = aLoggerFactory;
		_partitionColumn = aPartitionColumn;
		_isPartitionAutoInitialize = isPartitionAutoInitialize;
		if ( ( aDefaultLoggerName != null ) && ( aDefaultLoggerName.length() > 0 ) ) {
			_fallBackLogger = _loggerFactory.create( aDefaultLoggerName );
		}
		LOGGER.log( Level.FINE, "Using a partition column for type <" + aPartitionColumn.getType().getName() + "> with key \"" + aPartitionColumn.getKey() + "\"." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends T> aRecord ) {
		try {
			final L theLogger = getPartitionLogger( aRecord );
			theLogger.log( aRecord );
		}
		catch ( ColumnMismatchException e ) {
			throw new IllegalRecordRuntimeException( "Unable to retrieve partition column value from record!", aRecord, e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// PARTITIONING:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tests whether the provided partition exists.
	 * 
	 * @param aPartition The value of the {@link Column} in a {@link Record}
	 *        specifying the partition which is to be addressed (or even auto
	 *        initialized).
	 * 
	 * @return True in case the partition exists, else false.
	 */
	public boolean hasPartition( P aPartition ) {
		return _partitionToLoggerMap.containsKey( aPartition );
	}

	/**
	 * Initializes the given partition.
	 *
	 * @param aPartition The value of the {@link Column} in a {@link Record}
	 *        specifying the partition which is to be auto initialized.
	 * 
	 * @return the l
	 * 
	 * @throws InitializeException in case initialization failed e.g. when the
	 *         partition already exists or the system was not able to create the
	 *         required amount of endpoints.
	 */
	public synchronized L initPartition( P aPartition ) throws InitializeException {

		final String thePartitionIdentifier = getPartitionUid( aPartition );

		LOGGER.log( Level.FINE, "Initializing the partition \"" + ( aPartition == null ? "(null)" : thePartitionIdentifier ) + "\" ..." );

		if ( _partitionToLoggerMap.containsKey( aPartition ) ) {
			// @formatter:off
			throw new InitializeException( "A partition \"" + thePartitionIdentifier + "\" has already been initialized for column with key \"" + _partitionColumn.getKey() + "\" (with type \"" + _partitionColumn.getType().getName() + "\")." );
			// @formatter:on
		}
		final L theLogger = _loggerFactory.create( thePartitionIdentifier );
		ComponentUtility.initialize( theLogger );
		_partitionToLoggerMap.put( aPartition, theLogger );
		return theLogger;
	}

	/**
	 * Destroys the given partition. External resources might stay untouched! In
	 * case the partition does not exist, then there is no partition to be
	 * destroyed. To make things easier, this is not considered an exceptional
	 * state as after the call we expect that there is none such partition (any
	 * more) which is not destroyed.
	 *
	 * @param aPartition the partition
	 */
	public synchronized void destroyPartition( P aPartition ) {
		final L theLogger = _partitionToLoggerMap.remove( aPartition );
		if ( theLogger == null ) {
			return;
		}
		ComponentUtility.destroy( theLogger );
	}

	/**
	 * Flushes the given partition, all buffered data is to be forwarded to the
	 * physical data sink.
	 * 
	 * @param aPartition The partition to be flushed.
	 * 
	 * @exception IOException in case there were problems when flushing, e.g.
	 *            there is none such partition.
	 */
	public void flushPartition( P aPartition ) throws IOException {
		final L theLogger = _partitionToLoggerMap.get( aPartition );
		if ( theLogger == null ) {
			// @formatter:off
			throw new IOException( "A partition \"" + getPartitionUid( aPartition ) + "\" was not found for column with key \"" + _partitionColumn.getKey() + "\" (and type \"" + _partitionColumn.getType().getName() + "\")." );
			// @formatter:on
		}
		ComponentUtility.flush( theLogger );
	}

	/**
	 * Decomposes the given partition. External resources might get deleted
	 * (such as files or DB schemas)! In case the partition does not exist, then
	 * there is no partition to be decomposed. To make things easier, this is
	 * not considered an exceptional state as after the call we expect that
	 * there is none such partition (any more).
	 * 
	 * @param aPartition The partition to be decomposed.
	 */
	public void decomposePartition( P aPartition ) {
		final L theLogger = _partitionToLoggerMap.remove( aPartition );
		if ( theLogger == null ) {
			return;
		}
		if ( theLogger instanceof Decomposable ) {
			( (Decomposable) theLogger ).decompose();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Provides access to the partitions managed by the
	 * {@link AbstractPartedLogger}. This is ehttps://www.metacodes.proly useful
	 * for extensions of this class such as the
	 * {@link AbstractPartedQueryLogger} or the
	 * {@link AbstractPartedTrimLogger}.
	 * 
	 * @return The {@link Logger} instances managed by the
	 *         {@link AbstractPartedLogger} instance.
	 */
	protected Collection<L> getLoggers() {
		final Collection<L> result = _partitionToLoggerMap.values();
		if ( _fallBackLogger != null ) {
			result.add( _fallBackLogger );
		}
		return _partitionToLoggerMap.values();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the partition responsible for the provided {@link Record}.
	 *
	 * @param aRecord The {@link Record} for which to get the partition.
	 * 
	 * @return The {@link Logger} instance found responsible for the given
	 *         {@link Record} or null.
	 * 
	 * @throws ColumnMismatchException in case applying the partition
	 *         {@link Column} as provided to the constructor causes a problem
	 *         when applied to the given {@link Record}, e.g. no partition can
	 *         be retrieved from the {@link Record}
	 * @throws UnexpectedLogRuntimeException the unexpected log runtime
	 *         exception
	 * @throws IllegalRecordRuntimeException the illegal record runtime
	 *         exception
	 */
	private L getPartitionLogger( Record<?> aRecord ) throws ColumnMismatchException {
		final P thePartition = getPartition( aRecord );
		L theLogger;
		if ( thePartition == null && _fallBackLogger != null ) {
			theLogger = _fallBackLogger;
		}
		else {
			theLogger = _partitionToLoggerMap.get( thePartition );
		}

		if ( theLogger == null ) {
			if ( _isPartitionAutoInitialize ) {
				synchronized ( this ) {
					// To prevent thread race conditions:
					theLogger = _partitionToLoggerMap.get( thePartition );
					if ( theLogger != null ) {
						return theLogger;
					}
					try {
						theLogger = initPartition( thePartition );
					}
					catch ( InitializeException e ) {
						throw new UnexpectedLogRuntimeException( "Unable to initialize partition \"" + thePartition + "\" of record { " + TabularUtility.toSeparatedValues( aRecord, ',' ) + " } !", aRecord, e );
					}
				}
			}
			else {
				throw new IllegalRecordRuntimeException( "Unable to retrieve partition \"" + thePartition + "\" (auto create for partitions is diabled), perform the operation!", aRecord );
			}
		}
		return theLogger;
	}

	/**
	 * Determines the partition responsible for the provided {@link Record}.
	 *
	 * @param aRecord The {@link Record} for which to get the partition.
	 * 
	 * @return The partition instance found responsible for the given
	 *         {@link Record} or null.
	 * 
	 * @throws ColumnMismatchException Thrown in case applying the partition
	 *         {@link Column} as provided to the constructor causes a problem
	 *         when applied to the given {@link Record}.
	 * @throws IllegalRecordRuntimeException Thrown in case no partition
	 *         {@link Column} was found in the provided {@link Record}.
	 */
	@SuppressWarnings("unchecked")
	private P getPartition( Record<?> aRecord ) throws ColumnMismatchException {
		final Object theObj = _partitionColumn.get( aRecord );
		if ( theObj == null && _fallBackLogger == null ) {
			LOGGER.log( Level.WARNING, "An erronous record was provided: " + new VerboseTextBuilder().withElements( aRecord ).toString() );
			throw new IllegalRecordRuntimeException( "The record does not contain a partition column \"" + _partitionColumn.getKey() + "\" (\"" + _partitionColumn.getKey() + "\"), therefore no partition can be determined!", aRecord );
		}
		else if ( theObj == null ) {
			LOGGER.log( Level.FINE, "The partition column ('" + _partitionColumn.getKey() + "') not located in provided record, 'null' value returned." );
			return null;
		}

		LOGGER.log( Level.FINE, "Retrieved a partition of type <" + theObj.getClass().getName() + "> (\"" + theObj.toString() + "\") which is to be challenged with the type <" + _partitionColumn.getType().getName() + "> ..." );
		P thePartition = null;
		if ( _partitionColumn.getType().isAssignableFrom( theObj.getClass() ) ) {
			thePartition = (P) theObj;
		}
		LOGGER.log( Level.FINE, "Determined the partition \"" + ( thePartition == null ? "(null)" : getPartitionUid( thePartition ) ) + "\" for record the priovided record." );
		return thePartition;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Depending whether the partition object provides a Universal-TID or not we
	 * return the string representation of the partition's object or the UID.
	 * 
	 * @param aPartition The partition for which to get the identifier
	 * 
	 * @return The UID in case the partition provides a UID, else the
	 *         partition's string representation.
	 */
	protected String getPartitionUid( P aPartition ) {
		String thePartition = null;
		if ( aPartition != null ) {
			if ( aPartition instanceof UniversalIdAccessor ) {
				thePartition = ( (UniversalIdAccessor) aPartition ).getUniversalId();
			}
			else {
				thePartition = aPartition.toString();
			}
		}
		return thePartition;
	}

	/**
	 * Provides access to the {@link Column} used to identify partitions.
	 * 
	 * @return The {@link Column} identifying partitions.
	 */
	protected Column<P> getPartitionColumn() {
		return _partitionColumn;
	}

	/**
	 * Returns the {@link Logger} being responsible for the given partition.
	 * 
	 * @param aPartition The partition for which to get the responsible
	 *        {@link Logger}.
	 * 
	 * @return The {@link Logger} responsible for the given partition or null if
	 *         none was found for the partition.
	 */
	protected L getPartitionLogger( P aPartition ) {
		return _partitionToLoggerMap.get( aPartition );
	}

	/**
	 * Returns the fallback {@link Logger}.
	 * 
	 * @return The fallback logger or null if none was enabled.
	 */
	protected L getFallbackLogger() {
		return _fallBackLogger;
	}
}
