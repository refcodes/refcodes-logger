// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.tabular.Record;

/**
 * Implementation of the {@link AbstractLoggerFactoryComposite} creating
 * composite {@link QueryLogger} instances.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class QueryLoggerFactoryComposite<T> extends AbstractLoggerFactoryComposite<QueryLogger<T>> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new composite query logger factory impl.
	 *
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param aNumEndpoints the num endpoints
	 */
	public QueryLoggerFactoryComposite( LoggerFactory<QueryLogger<T>> aLoggerFactory, int aNumEndpoints ) {
		super( aLoggerFactory, aNumEndpoints );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected QueryLogger<T> createCompositeLogger( QueryLogger<T>[] aLoggers ) {
		return new QueryLoggerComposite<>( aLoggers );
	}
}
