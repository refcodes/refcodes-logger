// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.controlflow.InvocationStrategy;
import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.exception.BugException;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.Records;

/**
 * The {@link org.refcodes.logger.QueryLoggerComposite} extends the
 * {@link org.refcodes.logger.LoggerComposite} with the query logger
 * functionality. In contrast to the #log(Record) method, which is forwarded to
 * exactly one of the encapsulated query logger instances, the #findLogs()
 * method calls are forwarded to all of the encapsulated query logger instances.
 * (in case, partitioning is needed, take a look at the
 * {@link org.refcodes.logger.PartedQueryLogger})
 *
 * @param <L> The {@link Logger} type to be supported. As the
 *        {@link AbstractLoggerComposite} is being extended to support other
 *        {@link Logger} types, we have to provide this generic parameter (for
 *        example, sub-classes make use of the {@link #getLoggers()} method to
 *        access the encapsulated {@link Logger} instances being sub-types of
 *        the {@link Logger} interface).
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * 
 * @see QueryLoggerComposite
 */
abstract class AbstractQueryLoggerComposite<L extends QueryLogger<T>, T> extends AbstractLoggerComposite<L, T> implements QueryLogger<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractQueryLoggerComposite} from the provided
	 * {@link Logger} instances.
	 * 
	 * @param aLoggers The {@link Logger} instances to be used for the
	 *        {@link AbstractQueryLoggerComposite}.
	 */
	@SafeVarargs
	public AbstractQueryLoggerComposite( L... aLoggers ) {
		super( aLoggers );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs() {
		try {
			return findLogs( null, null, -1 );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( int aLimit ) {
		try {
			return findLogs( null, null, aLimit );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( final Criteria aCriteria ) {
		try {
			return findLogs( aCriteria, null, -1 );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Header<T> aHeader, int aLimit ) {
		try {
			return findLogs( null, aHeader, aLimit );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( final Criteria aCriteria, int aLimit ) throws BadCriteriaException {
		return findLogs( aCriteria, null, aLimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( final Criteria aCriteria, final Header<T> aHeader ) throws BadCriteriaException {
		return findLogs( aCriteria, aHeader, -1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Criteria aCriteria, Header<T> aHeader, int aLimit ) throws BadCriteriaException {
		return LoggerUtility.findLogs( aCriteria, aHeader, aLimit, getLoggers(), InvocationStrategy.ROUND_ROBIN );
	}
}
