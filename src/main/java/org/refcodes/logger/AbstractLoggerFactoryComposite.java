// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Map;
import java.util.logging.Level;

/**
 * This {@link AbstractLoggerFactoryComposite} is a {@link LoggerFactory} which
 * creates composite {@link Logger} instances, which themselves encapsulate
 * {@link Logger} instances. The factory methods {@link #create(String)} and
 * {@link #create(String)} actually return composite {@link Logger} instances
 * which in turn contain {@link Logger} instances being created by the provided
 * {@link LoggerFactory}.
 *
 * @param <L> The type (sub-type) of the {@link Logger} to be created.
 */
abstract class AbstractLoggerFactoryComposite<L extends Logger<?>> implements LoggerFactory<L> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AbstractLoggerFactoryComposite.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _numEndpoints;
	private final LoggerFactory<L> _loggerFactory;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link AbstractLoggerFactoryComposite} specifying the number
	 * of {@link Logger} instances to be encapsulated by the created composites
	 * to when invoking the methods {@link #create(String)} or
	 * {@link #create(String, Map)}. The provided {@link LoggerFactory} (as
	 * passed to the constructor) actually creates the {@link Logger} instances
	 * encapsulated in the composite, backed by a physical data sink (or other
	 * composite or parted {@link Logger} instances).
	 * 
	 * @param aLoggerFactory The {@link LoggerFactory} to create the (atomic)
	 *        {@link Logger} instances (or other composite or parted
	 *        {@link Logger} instances).
	 * @param aNumEndpoints The number of {@link Logger} instances to be
	 *        included for each composite returned by the
	 *        {@link #create(String)} or the {@link #create(String, Map)}
	 *        methods.
	 */
	public AbstractLoggerFactoryComposite( LoggerFactory<L> aLoggerFactory, int aNumEndpoints ) {
		if ( aNumEndpoints <= 0 ) {
			throw new IllegalArgumentException( "The argument \"aNumEndpoints\" must be > 0, you provided a value of <" + aNumEndpoints + ">!" );
		}
		_numEndpoints = aNumEndpoints;
		_loggerFactory = aLoggerFactory;
		LOGGER.log( Level.FINE, "Got a Logger factory of type \"" + ( aLoggerFactory != null ? aLoggerFactory.getClass().getName() : null ) + "\"." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a compound {@link Logger} containing {@link Logger} instances
	 * fabricated by the provided {@link LoggerFactory} (as passed to the
	 * constructor). The provided schema body is used when creating the
	 * {@link Logger} instances with this {@link LoggerFactory}. When creating
	 * those {@link Logger} instances by the provided {@link LoggerFactory} (as
	 * passed to the constructor), the schema body is suffixed by a '.###' for
	 * each instance where "###" represents the actual number of an individual
	 * instance. Numbers reach from '000' to the actual number of endpoints
	 * provided to the constructor.
	 * 
	 * @param aSchemaBody The 'schema'# to be used by the {@link Logger}
	 *        instances created by the provided {@link LoggerFactory} (as passed
	 *        to the constructor). The schema body may be part of a DB schema
	 *        name or a SimpleDB domain name, this depends on the actual
	 *        {@link Logger} implementation returned by the
	 *        {@link LoggerFactory} (as passed to the constructor).
	 * 
	 * @return The composite {@link Logger} instance as fabricated by this
	 *         {@link AbstractLoggerFactoryComposite}.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public L create( String aSchemaBody ) {
		final L[] theLoggers = (L[]) new Logger[getNumEndpoints()];
		String eSchemaName;
		for ( int i = 0; i < theLoggers.length; i++ ) {
			eSchemaName = LoggerUtility.getSchemaName( aSchemaBody, i );
			LOGGER.log( Level.FINE, "Creating Logger <" + i + "> instance for schema \"" + eSchemaName + "\"..." );
			theLoggers[i] = getLoggerFactory().create( eSchemaName );
		}
		return createCompositeLogger( theLoggers );
	}

	/**
	 * Creates a compound {@link Logger} containing {@link Logger} instances
	 * fabricated by the provided {@link LoggerFactory} (as passed to the
	 * constructor). The provided schema body is used when creating the
	 * {@link Logger} instances with this {@link LoggerFactory}. When creating
	 * those {@link Logger} instances by the provided {@link LoggerFactory} (as
	 * passed to the constructor), the schema body is suffixed by a '.###' for
	 * each instance where "###" represents the actual number of an individual
	 * instance. Numbers reach from '000' to the actual number of endpoints
	 * provided to the constructor.
	 * 
	 * @param aSchemaBody The 'schema'# to be used by the {@link Logger}
	 *        instances created by the provided {@link LoggerFactory} (as passed
	 *        to the constructor). The schema body may be part of a DB schema
	 *        name or a SimpleDB domain name, this depends on the actual
	 *        {@link Logger} implementation returned by the
	 *        {@link LoggerFactory} (as passed to the constructor).
	 * @param aProperties Properties which may be used to pass additional
	 *        configuration settings to the {@link LoggerFactory} (as passed to
	 *        the constructor) when creating the encapsulated {@link Logger}
	 *        instances.
	 * 
	 * @return The composite {@link Logger} instance as fabricated by this
	 *         {@link AbstractLoggerFactoryComposite}.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public L create( String aSchemaBody, Map<String, String> aProperties ) {
		final L[] theLoggers = (L[]) new Logger[getNumEndpoints()];
		String eSchemaName;
		for ( int i = 0; i < theLoggers.length; i++ ) {
			eSchemaName = LoggerUtility.getSchemaName( aSchemaBody, i );
			LOGGER.log( Level.FINE, "Creating Logger <" + i + "> instance for schema \"" + eSchemaName + "\"..." );
			theLoggers[i] = getLoggerFactory().create( eSchemaName, aProperties );
		}
		return createCompositeLogger( theLoggers );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To be implemented by sub-classes, this method creates a
	 * {@link LoggerComposite} (or its sub-classes) from the provided
	 * {@link Logger} instances (or its sub-types).
	 * 
	 * @param aLoggers The {@link Logger} instances (or its sub-types) to be
	 *        contained in the composite.
	 * 
	 * @return The composite containing the given {@link Logger} instances.
	 */
	protected abstract L createCompositeLogger( L[] aLoggers );

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the number if {@link Logger} instances to be created when
	 * creating a composite {@link Logger}.
	 * 
	 * @return The number of {@link Logger} instances to be contained in the
	 *         composite {@link Logger}.
	 */
	protected int getNumEndpoints() {
		return _numEndpoints;
	}

	/**
	 * The {@link LoggerFactory} which to use in order to create the
	 * encapsulated {@link Logger} instances contained in the composite
	 * {@link Logger} instances created by the {@link #create(String)} or
	 * {@link #create(String, Map)} methods.
	 * 
	 * @return The {@link LoggerFactory} to be used by the
	 *         {@link AbstractLoggerFactoryComposite} when creating the
	 *         encapsulated {@link Logger} instances to be contained in the
	 *         composite {@link Logger} instances..
	 */
	protected LoggerFactory<L> getLoggerFactory() {
		return _loggerFactory;
	}
}
