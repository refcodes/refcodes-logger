// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.refcodes.component.Destroyable;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.RetryCount;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.exception.Trap;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.Records;

/**
 * The {@link org.refcodes.logger.LoggerComposite} uses the composite pattern to
 * forward logger functionality to a number encapsulated logger instances.
 * Depending on the performance (and availability) of an encapsulated logger,
 * the calls to the composite's #log(Record) method are executed by the next
 * encapsulated logger ready for execution.
 * <p>
 * An invocation of the
 * {@link org.refcodes.logger.LoggerComposite#log(org.refcodes.tabular.Record)}
 * method is forwarded to exactly one of the encapsulated
 * {@link org.refcodes.logger.Logger} instances. The actual instance being
 * called depends on its availability (in case, partitioning is needed, take a
 * look at the {@link org.refcodes.logger.PartedLogger} and its sub-classes).
 * <p>
 * Using the {@link org.refcodes.logger.LoggerComposite}, a huge number of
 * {@link org.refcodes.tabular.Record} instances can be logged in parallel by
 * logging them to different physical data sinks (represented by the
 * encapsulated logger instances), thereby avoiding a bottleneck which a single
 * physical data sink would aCause for logging.
 * <p>
 * Internally a log line queue (holding {@link org.refcodes.tabular.Record}
 * instances to be logged) as well a daemon thread per encapsulated logger
 * (taking elements from the log line queue) are used to decouple the
 * encapsulated logger instances from the
 * {@link org.refcodes.logger.LoggerComposite}.
 * <p>
 * A given number of retries are approached in case there is an overflow of the
 * log line queue; this happens when the queue is full and there are none
 * encapsulated logger instances to take the next
 * {@link org.refcodes.tabular.Record}.
 * <p>
 * To avoid a building up of the log line queue, eventually causing an out of
 * memory, log lines not being taken into the log line queue (as it is full)
 * within the given number of retries, them log lines are dismissed. In such a
 * case a warning with a log-level WARN is printed out. Idea: The
 * {@link LoggerComposite} could implement observable functionality as of the
 * "refcodes-observer" artifact; firing events when the log lines queue grows
 * near its limits - action then can be taken to add loggers to the
 * {@link LoggerComposite}. Also when the log line queue shrinks, loggers could
 * be removed from the {@link LoggerComposite} (important in cloud environments
 * where resources cost real money).
 * <p>
 * Idea: Provide configuration parameters (additional constructors) for not
 * dismissing any log lines with the risk of a memory overflow. Also we might
 * provide parameters in additional constructors specifying minimum and maximum
 * number of endpoints; depending on the fill level of the log line queue, the
 * number of encapsulated {@link Logger} instances might get increased (then we
 * need a {@link LoggerFactory} also being provided) or decreased.
 *
 * @param <L> The {@link Logger} type to be supported. As the
 *        {@link AbstractLoggerComposite} is being extended to support other
 *        {@link Logger} types, we have to provide this generic parameter (for
 *        example, sub-classes make use of the {@link #getLoggers()} method to
 *        access the encapsulated {@link Logger} instances being sub-types of
 *        the {@link Logger} interface).
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * 
 * @see LoggerComposite
 */
abstract class AbstractLoggerComposite<L extends Logger<T>, T> implements Logger<T>, Destroyable {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AbstractLoggerComposite.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOGGERS_MULTIPLIER = 1000;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LinkedBlockingQueue<Record<? extends T>> _logLineQueue;
	private final List<L> _loggers = new ArrayList<>();
	private boolean _isDestroyed = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractLoggerComposite} from the provided
	 * {@link Logger} instances.
	 * 
	 * @param aLoggers The {@link Logger} instances to be used for the
	 *        {@link AbstractLoggerComposite}.
	 */
	@SafeVarargs
	public AbstractLoggerComposite( L... aLoggers ) {
		this( ControlFlowUtility.createCachedExecutorService( true ), aLoggers );
	}

	/**
	 * Constructs an {@link AbstractLoggerComposite} from the provided
	 * {@link Logger} instances.
	 * 
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads.
	 * @param aLoggers The {@link Logger} instances to be used for the
	 *        {@link AbstractLoggerComposite}.
	 */
	@SafeVarargs
	public AbstractLoggerComposite( ExecutorService aExecutorService, L... aLoggers ) {

		if ( aLoggers == null ) {
			throw new IllegalArgumentException( "Unable to construct the composite logger as there must at least one logger instance provided!" );
		}
		_logLineQueue = new LinkedBlockingQueue<>( aLoggers.length * LOGGERS_MULTIPLIER );
		_loggers.addAll( Arrays.asList( aLoggers ) );

		// -------------------
		// Create the daemons:
		// -------------------
		LogDaemon eDaemon = null;
		for ( L eLogger : aLoggers ) {
			eDaemon = new LogDaemon( eLogger );
			aExecutorService.execute( eDaemon );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends T> aRecord ) {
		// ---------------------------------------------------------------------
		// We assume to log to physical data sinks, therefore we use IO retries.
		// ---------------------------------------------------------------------
		final RetryCounter theRetryCounter = new RetryCounter( IoRetryCount.MAX.getValue() );
		try {
			while ( !_logLineQueue.offer( aRecord, SleepLoopTime.MAX.getTimeMillis(), TimeUnit.MILLISECONDS ) && theRetryCounter.nextRetry() ) {
				LOGGER.log( Level.WARNING, "Trying to offer (add) a log line to the log line queue, though the queue is full, this is retry # <" + theRetryCounter.getRetryCount() + ">, aborting after <" + theRetryCounter.getRetryNumber() + "> retries. Retrying now after a delay of <" + ( SleepLoopTime.MAX.getTimeMillis() / 1000 ) + "> seconds..." );
				if ( !theRetryCounter.hasNextRetry() ) {
					throw new UnexpectedLogRuntimeException( "Unable to process the log line after <" + theRetryCounter.getRetryNumber() + "> retries, aborting retries, dismissing log line \"" + aRecord.toString() + "\"!", aRecord );
				}
			}
		}
		catch ( InterruptedException ignored ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_isDestroyed = true;
		final RetryCounter theRetryCounter = new RetryCounter( RetryCount.MAX.getValue(), SleepLoopTime.NORM.getTimeMillis() );
		while ( theRetryCounter.hasNextRetry() && !_logLineQueue.isEmpty() ) {
			LOGGER.log( Level.WARNING, "The logline queue is not empty, waiting <" + theRetryCounter.getNextRetryDelayMillis() + "> ms for next retry number <" + theRetryCounter.getRetryCount() + "> (of <" + theRetryCounter.getRetryNumber() + "> altogether)." );
			theRetryCounter.nextRetry();
		}
		final int theLogLineQueueSize = _logLineQueue.size();
		if ( theLogLineQueueSize != 0 ) {
			LOGGER.log( Level.WARNING, "The logline queue was not empty (with size <" + theLogLineQueueSize + ">) upon destroying this component." );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the loggers.
	 *
	 * @return the loggers
	 */
	protected Collection<L> getLoggers() {
		return _loggers;
	}

	// /////////////////////////////////////////////////////////////////////////
	// DEAMONS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The LogDaemon is a daemon thread taking log line {@link Record} instances
	 * from the log line queue to pass them to a dedicated {@link Logger}
	 * instance. Many LogDaemon threads operate on the same log line queue
	 * feeding the log line {@link Records} instances each to a dedicated
	 * {@link Logger} instance in parallel.
	 */
	private class LogDaemon implements Runnable {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final L _logger;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs LogDeaon responsible for feeding log {@link Record}
		 * instances from a log line queue to its encapsulated {@link Logger}..
		 * 
		 * @param aLogger The logger fed with log {@link Record} instances from
		 *        the log line queue.
		 */
		public LogDaemon( L aLogger ) {
			_logger = aLogger;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Run.
		 */
		@Override
		public void run() {
			Record<? extends T> eRecord = null;
			while ( !_logLineQueue.isEmpty() || !_isDestroyed ) {
				try {
					eRecord = _logLineQueue.take();
					_logger.log( eRecord );
				}
				catch ( InterruptedException e ) {
					break;
				}
				catch ( IllegalRecordRuntimeException e ) {
					LOGGER.log( Level.WARNING, "As of an illegal record, the daemon is unable to log the log line \"" + eRecord + "\" as of: " + Trap.asMessage( e ), e );
				}
				catch ( UnexpectedLogRuntimeException e ) {
					LOGGER.log( Level.WARNING, "As of an unexpected log exception, the daemon is unable to log the log line \"" + eRecord + "\" as of: " + Trap.asMessage( e ), e );
				}
				catch ( Exception e ) {
					LOGGER.log( Level.WARNING, "As of an unrecognized exception, the daemon is unable to log the log line \"" + eRecord + "\" as of: " + Trap.asMessage( e ), e );
				}
			}
		}
	}
}
