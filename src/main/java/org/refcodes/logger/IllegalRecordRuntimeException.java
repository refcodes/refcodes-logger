// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.logger.LoggerRuntimeException.LoggerRecordRuntimeException;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.Record;

/**
 * Thrown in case the record cannot be logged as a specific implementation might
 * expect some dedicated {@link Column} instances to be contained in the
 * provided Record.
 */
public class IllegalRecordRuntimeException extends LoggerRecordRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public IllegalRecordRuntimeException( String aMessage, Record<?> aRecord, String aErrorCode ) {
		super( aMessage, aRecord, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public IllegalRecordRuntimeException( String aMessage, Record<?> aRecord, Throwable aCause, String aErrorCode ) {
		super( aMessage, aRecord, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public IllegalRecordRuntimeException( String aMessage, Record<?> aRecord, Throwable aCause ) {
		super( aMessage, aRecord, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public IllegalRecordRuntimeException( String aMessage, Record<?> aRecord ) {
		super( aMessage, aRecord );
	}

	/**
	 * {@inheritDoc}
	 */
	public IllegalRecordRuntimeException( Record<?> aRecord, Throwable aCause, String aErrorCode ) {
		super( aRecord, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public IllegalRecordRuntimeException( Record<?> aRecord, Throwable aCause ) {
		super( aRecord, aCause );
	}
}
