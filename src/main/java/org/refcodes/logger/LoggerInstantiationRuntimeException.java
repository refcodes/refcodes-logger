// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.mixin.FilenameAccessor;
import org.refcodes.mixin.NamespaceAccessor;

/**
 * Thrown in case instantiating a {@link Logger} ({@link RuntimeLogger}) failed,
 * e.g. the {@link RuntimeLoggerSingleton} tries to construct a
 * {@link RuntimeLogger} from a bean configuration file (or a
 * SpringRuntimeLoggerSingleton tries to construct a {@link RuntimeLogger} from
 * a bean configuration context file) and fails doing so (due to missing or
 * "wrong" configuration file).
 */
public class LoggerInstantiationRuntimeException extends LoggerRuntimeException implements FilenameAccessor, NamespaceAccessor {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	protected String _namespace;
	protected String _filename;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public LoggerInstantiationRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerInstantiationRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerInstantiationRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerInstantiationRuntimeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerInstantiationRuntimeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LoggerInstantiationRuntimeException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aFilename The filename involved in this exception.
	 * @param aNamespace The namespace involved in this exception.
	 */
	public LoggerInstantiationRuntimeException( String aMessage, String aFilename, String aNamespace, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_filename = aFilename;
		_namespace = aNamespace;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aFilename The filename involved in this exception.
	 * @param aNamespace The namespace involved in this exception.
	 */
	public LoggerInstantiationRuntimeException( String aMessage, String aFilename, String aNamespace, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_filename = aFilename;
		_namespace = aNamespace;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aFilename The filename involved in this exception.
	 * @param aNamespace The namespace involved in this exception.
	 */
	public LoggerInstantiationRuntimeException( String aMessage, String aFilename, String aNamespace, Throwable aCause ) {
		super( aMessage, aCause );
		_filename = aFilename;
		_namespace = aNamespace;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aFilename The filename involved in this exception.
	 * @param aNamespace The namespace involved in this exception.
	 */
	public LoggerInstantiationRuntimeException( String aMessage, String aFilename, String aNamespace ) {
		super( aMessage );
		_filename = aFilename;
		_namespace = aNamespace;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aFilename The filename involved in this exception.
	 * @param aNamespace The namespace involved in this exception.
	 */
	public LoggerInstantiationRuntimeException( String aFilename, String aNamespace, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_filename = aFilename;
		_namespace = aNamespace;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aFilename The filename involved in this exception.
	 * @param aNamespace The namespace involved in this exception.
	 */
	public LoggerInstantiationRuntimeException( String aFilename, String aNamespace, Throwable aCause ) {
		super( aCause );
		_filename = aFilename;
		_namespace = aNamespace;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getNamespace() {
		return _namespace;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFilename() {
		return _filename;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _filename, _namespace };
	}
}
