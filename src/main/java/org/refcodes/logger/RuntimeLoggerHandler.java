// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * The {@link RuntimeLoggerHandler} provides a {@link Handler} for the JUL
 * Framework.
 */
public class RuntimeLoggerHandler extends Handler {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Set<String> _julLoggerNames = Collections.newSetFromMap( new WeakHashMap<>() );

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publish( LogRecord aRecord ) {
		final String theLoggerName = aRecord.getLoggerName();
		final RuntimeLogger theRuntimeLogger = RuntimeLoggerFactorySingleton.createRuntimeLogger( theLoggerName );
		if ( !_julLoggerNames.contains( theLoggerName ) ) {
			synchronized ( _julLoggerNames ) {
				if ( !_julLoggerNames.contains( theLoggerName ) ) {
					_julLoggerNames.add( theLoggerName );
					theRuntimeLogger.log( LogPriority.DEBUG, "Created JUL logger <" + theLoggerName + "> ..." );
				}
			}
		}
		theRuntimeLogger.log( aRecord );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {}
}
