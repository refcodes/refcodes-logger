// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import org.refcodes.controlflow.InvocationStrategy;
import org.refcodes.criteria.AndCriteria;
import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.criteria.CriteriaException;
import org.refcodes.criteria.CriteriaUtility;
import org.refcodes.criteria.EqualWithCriteria;
import org.refcodes.criteria.ExpressionQueryFactory;
import org.refcodes.criteria.OrCriteria;
import org.refcodes.exception.BugException;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.Records;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * The {@link AbstractPartedQueryLogger} extends the
 * {@link AbstractPartedLogger} with the functionality of a {@link QueryLogger}.
 * Any query operations, such as {@link #findLogs(Criteria)}, are targeted at
 * that partition containing the queried data. For this to work, the query must
 * obey some rules: The query is to contain an {@link EqualWithCriteria}
 * addressing the partition in an unambiguous way; by being part of a root level
 * {@link AndCriteria} or an unambiguously nested {@link AndCriteria} hierarchy.
 * More than one partition gets detected when unambiguous {@link OrCriteria} are
 * applied to the partition {@link Criteria}. In such cases, the query is
 * addressed to the potential partitions. In case it was not possible to
 * identify any partitions, then as a fallback, all partitions are queried.
 * Query results are taken from from the the invoked partitions (in normal cases
 * this would be a single partition) round robin. the first result is taken from
 * the first queried partition's result set ({@link Records}), the next result
 * from the next queried partition and so on to start over again with the first
 * queried partition. Round robin has been used to prevent invalidation of
 * physical data sinks's result sets as of timeouts. (you may specify the actual
 * {@link Record} type (generic parameter) accepted by the {@link QueryLogger}
 * instances to enforce a dedicated {@link Record} type which contains the
 * required partition {@link Column})
 *
 * @param <L> The type of the {@link QueryLogger} to be created.
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * @param <P> The type of the {@link Column}'s value used for partitioning the
 *        {@link QueryLogger}.
 */
abstract class AbstractPartedQueryLogger<L extends QueryLogger<T>, T, P extends T> extends AbstractPartedLogger<L, T, P> implements QueryLogger<T> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AbstractPartedQueryLogger.class.getName() );
	private static final InvocationStrategy RETRIEVAL_STRATEGY = InvocationStrategy.ROUND_ROBIN;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract parted query logger.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 * 
	 * @see AbstractPartedLogger#AbstractPartedLogger(Column, LoggerFactory,
	 *      boolean)
	 */
	public AbstractPartedQueryLogger( Column<P> aPartitionColumn, LoggerFactory<L> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aLoggerFactory, isPartitionAutoInitialize );
	}

	/**
	 * Instantiates a new abstract parted query logger.
	 *
	 * @param aPartitionColumn the {@link Column} identifying partition column
	 *        for sharding.
	 * @param aDefaultLoggerName In case a fallback {@link Logger} is to be used
	 *        when no partition can be determined, then this parameter defines
	 *        the name of the fallback {@link Logger}.
	 * @param aLoggerFactory the {@link LoggerFactory} populating this
	 *        composite.
	 * @param isPartitionAutoInitialize True in case a partition not yet
	 *        existing is to be created on the fly, e.g. in case a log is
	 *        applied against a non existing partition, then the
	 *        {@link #initPartition(Object)} method is invoked upon this
	 *        partition. Find and delete operations are not considered.
	 * 
	 * @see AbstractPartedLogger#AbstractPartedLogger(Column, String,
	 *      LoggerFactory, boolean)
	 */
	public AbstractPartedQueryLogger( Column<P> aPartitionColumn, String aDefaultLoggerName, LoggerFactory<L> aLoggerFactory, boolean isPartitionAutoInitialize ) {
		super( aPartitionColumn, aDefaultLoggerName, aLoggerFactory, isPartitionAutoInitialize );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs() {
		try {
			return findLogs( null, null, -1 );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( int aLimit ) {
		try {
			return findLogs( null, null, aLimit );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( final Criteria aCriteria ) {
		try {
			return findLogs( aCriteria, null, -1 );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Header<T> aHeader, int aLimit ) {
		try {
			return findLogs( null, aHeader, aLimit );
		}
		catch ( BadCriteriaException e ) {
			throw new BugException( "We must not get this exception <" + e.getClass().getName() + "> as no criteria has been provided: ", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( final Criteria aCriteria, int aLimit ) throws BadCriteriaException {
		return findLogs( aCriteria, null, aLimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( final Criteria aCriteria, final Header<T> aHeader ) throws BadCriteriaException {
		return findLogs( aCriteria, aHeader, -1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Criteria aCriteria, Header<T> aHeader, int aLimit ) throws BadCriteriaException {

		final Set<P> thePartitions = getPartitions( aCriteria );

		// ---------------------------------------------------------------------
		// Single partition to be addressed:
		// ---------------------------------------------------------------------
		if ( thePartitions.size() == 1 ) {
			final P ePartition = thePartitions.iterator().next();
			final L theLogger = getPartitionLogger( ePartition );
			if ( theLogger != null ) {
				// Criteria, , aPartitions
				aCriteria = CriteriaUtility.doRemovePartitionCriteria( aCriteria, getPartitionColumn().getKey(), thePartitions );
				return theLogger.findLogs( aCriteria, aHeader, aLimit );
			}
			LOGGER.log( Level.WARNING, "No logger found for partition \"" + getPartitionUid( ePartition ) + "\": Now querying all partitions (fallback)!" );
		}
		else if ( thePartitions.size() == 0 && getFallbackLogger() != null ) {
			LOGGER.log( Level.FINE, "No partition found for provided criteria, now querying default logger instance to find logs in question!" );
			return getFallbackLogger().findLogs( aCriteria, aHeader, aLimit );
		}

		// #####################################################################
		// In happy cases we should end the query here! Examine the logs in
		// order to determine on how to improve your partitioning (partitioning)
		// or your queries in order to always target at just one partition!
		// #####################################################################

		final Collection<L> theLoggers = getPartitionLoggers( thePartitions );

		// -------------
		// Do the query:
		// -------------
		return LoggerUtility.findLogs( aCriteria, aHeader, aLimit, theLoggers, RETRIEVAL_STRATEGY );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves a collection with {@link Logger} instances managing the
	 * provided partitions.
	 * 
	 * @param aPartitions The partitions for which to get the {@link Logger}
	 *        instances.
	 * 
	 * @return A {@link Collection} with none to many {@link Logger} instances
	 */
	protected Collection<L> getPartitionLoggers( Set<P> aPartitions ) {

		Collection<L> theLoggers;
		// ---------------------------------------------------------------------
		// None partitions detected at all:
		// ---------------------------------------------------------------------
		if ( aPartitions.isEmpty() ) {
			theLoggers = getLoggers();
			LOGGER.log( Level.WARNING, "None partitions at all detected for the query (criteria): Now querying all partitions (fallback)!" );
		}
		else {
			// -----------------------------------------------------------------
			// Multiple partitions to be addressed:
			// -----------------------------------------------------------------
			theLoggers = new HashSet<>();
			if ( aPartitions.size() != 1 ) {
				L ePartitionLogger;
				for ( P ePartition : aPartitions ) {
					ePartitionLogger = getPartitionLogger( ePartition );
					if ( ePartitionLogger != null ) {
						theLoggers.add( ePartitionLogger );
					}
					else {
						LOGGER.log( Level.WARNING, "Determined not exisiting partition " + getPartitionUid( ePartition ) + " to be addressed for the query (criteria), no logger instance present for that partition! Now querying all partitions (fallback)!" );
					}
				}
			}

			// -----------------------------------------------------------------
			// All partitions to be addressed:
			// -----------------------------------------------------------------
			if ( theLoggers.isEmpty() || theLoggers.size() != aPartitions.size() ) {
				theLoggers = getLoggers();
				LOGGER.log( Level.WARNING, "None existing partitions detected (partitions with no logger) for the query (criteria): Now querying all partitions (fallback)!" );
			}
		}
		return theLoggers;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tries to determine the partitions being addressed by the query
	 * represented by the given {@link Criteria}.
	 * -------------------------------------------------------------------------
	 * CAUTION: The query is to contain an {@link EqualWithCriteria} addressing
	 * the partition in an unambiguous way; by being part of a root level
	 * {@link AndCriteria} or an unambiguously nested {@link AndCriteria}
	 * hierarchy. More than one partition gets detected when unambiguous
	 * {@link OrCriteria} are applied to the partition {@link Criteria}. In such
	 * cases, the query is addressed to the potential partitions.
	 * -------------------------------------------------------------------------
	 * 
	 * @param aCriteria The {@link Criteria} query from which to determine the
	 *        partitions.
	 * 
	 * @return A {@link Set} containing the detected partitions.
	 * 
	 * @throws CriteriaException
	 */
	private Set<P> getPartitions( Criteria aCriteria ) throws BadCriteriaException {
		final Set<P> thePartitions = CriteriaUtility.getPartitions( aCriteria, getPartitionColumn().getKey(), (Class<P>) getPartitionColumn().getType() );

		if ( LOGGER.isLoggable( Level.FINE ) ) {
			final String theQuery;
			if ( aCriteria == null ) {
				theQuery = "(null)";
			}
			else {
				final ExpressionQueryFactory theExpressionQueryFactory = new ExpressionQueryFactory();
				theQuery = theExpressionQueryFactory.fromCriteria( aCriteria );
			}
			LOGGER.log( Level.FINE, "Partitions " + new VerboseTextBuilder().withElements( thePartitions ).toString() + " determined for (raw) query is: \"" + theQuery + "\"" );
		}

		if ( thePartitions.size() > 1 ) {
			LOGGER.log( Level.WARNING, "Determined <" + thePartitions.size() + "> partitions to be addresses by the query (criteria), the number of partitions to be targeted at is greater than one, probably you have a bad partitioning (partitioning) or query (criteria) strategy!" );

		}
		return thePartitions;
	}
}
