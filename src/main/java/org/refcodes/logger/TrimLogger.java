// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.mixin.Clearable;
import org.refcodes.tabular.Record;

/**
 * The {@link TrimLogger} extends the {@link QueryLogger} with the functionality
 * to remove {@link Record} instances previously being logged by providing the
 * according {@link Criteria}.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public interface TrimLogger<T> extends QueryLogger<T>, Clearable {

	/**
	 * Deletes logged {@link Record} instances according to the provided
	 * {@link Criteria}.
	 * 
	 * @param aCriteria The {@link Criteria} to be applied when deleting
	 *        previously logged {@link Record} instances.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	void deleteLogs( Criteria aCriteria ) throws BadCriteriaException;

}