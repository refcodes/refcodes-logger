// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.tabular.Column;
import org.refcodes.tabular.Record;

/**
 * The {@link Logger} interface defines those methods required for an atomic
 * (sub-)logger to implement. Specialized loggers may make use of {@link Logger}
 * implementations; for example the {@link RuntimeLogger} can be configured to
 * log to a console, a NoSQL database or a file depending on how it is
 * configured (on which {@link Logger} implementation it is told to use).
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public interface Logger<T> extends LogDecorator {

	/**
	 * Logs a {@link Record}. The targeted data sink for the {@link Record}
	 * instances (where them are physically stored) depends on the
	 * implementation of the {@link Logger}. It can be a console, a file, a
	 * stream or a database.
	 * 
	 * @param aRecord The {@link Record} to be logged.
	 * 
	 * @throws IllegalRecordRuntimeException Thrown in case the record cannot be
	 *         logged as a specific implementation might expect some dedicated
	 *         {@link Column} instances to be contained in the provided Record.
	 * @throws UnexpectedLogRuntimeException Thrown in case some other problems
	 *         regarding logging occurred, e.g. the data sink (physical system
	 *         where to log to) experiences problems.
	 */
	void log( Record<? extends T> aRecord );
}
