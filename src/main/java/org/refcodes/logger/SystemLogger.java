// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger;

import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.Record;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

/**
 * Very plain implementation of the logger interface, mainly used as fallback
 * {@link Logger}.
 */
public class SystemLogger implements Logger<Object> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<?> aRecord ) {
		try {
			final LogPriority theLogPriority = (LogPriority) LoggerField.LOG_PRIORITY.getColumn().get( aRecord );
			if ( theLogPriority.getPriority() >= LogPriority.WARN.getPriority() ) {
				System.err.println( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( aRecord.values() ).toRecord() );
			}
			else {
				System.out.println( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( aRecord.values() ).toRecord() );
			}
			return;
		}
		catch ( ColumnMismatchException | ClassCastException e ) {
			/* ignore */
		}
		System.out.println( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( aRecord.values() ).toRecord() );

	}
}
