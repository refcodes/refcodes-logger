// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import java.util.logging.Level;

/**
 * This enumeration defines the various log priorities to be used by the
 * application logger. Them log priorities are inspired by common loggers such a
 * slog4jand the Syslog priorities. Them priorities have the following meaning
 * as of "http://en.wikipedia.org/wiki/Syslog":
 */
public enum LogPriority {

	/**
	 * "A "panic" condition usually affecting multiple apps/servers/sites. At
	 * this level it would usually notify all tech staff on call." ( See also
	 * http://en.wikipedia.org/wiki/Syslog
	 */
	PANIC(8),

	/**
	 * "Should be corrected immediately, therefore notify staff who can fix the
	 * problem. An example would be the loss of a primary ISP connection." See
	 * also http://en.wikipedia.org/wiki/Syslog
	 */
	ALERT(7),

	/**
	 * "Should be corrected immediately, but indicates failure in a secondary
	 * system, an example is a loss of a backup ISP connection." See also
	 * http://en.wikipedia.org/wiki/Syslog
	 */
	CRITICAL(6),

	/**
	 * "Non-urgent failures, these should be relayed to developers or admins;
	 * each item must be resolved within a given time." See also
	 * http://en.wikipedia.org/wiki/Syslog
	 */
	ERROR(5),

	/**
	 * "Warning messages, not an error, but indication that an error will occur
	 * if action is not taken, e.g. file system 85% full - each item must be
	 * resolved within a given time." See also
	 * http://en.wikipedia.org/wiki/Syslog
	 */
	WARN(4),

	/**
	 * "Events that are unusual but not error conditions - might be summarized
	 * in an email to developers or admins to spot potential problems - no
	 * immediate action required." See also http://en.wikipedia.org/wiki/Syslog
	 */
	NOTICE(3),

	/**
	 * "Additional information which might be useful for some stability period."
	 * See also http://en.wikipedia.org/wiki/Syslog
	 */
	INFO(2),

	/**
	 * "Info useful to developers for debugging the application, not useful
	 * during operations." See also http://en.wikipedia.org/wiki/Syslog
	 */
	DEBUG(1),

	/**
	 * "Info useful to developers for debugging the application, not useful
	 * during operations." See also http://en.wikipedia.org/wiki/Syslog TRACE
	 * provides even more comments for the developer when debugging.
	 */

	TRACE(0),
	/**
	 * Log is deactivated.
	 */
	DISCARD(-1),

	/**
	 * No priority (has been set yet).
	 */
	NONE(-2);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _priority;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a log priority with the given severity. The severity specifies
	 * the severity of a log value. The smaller the severity, the less severe is
	 * a log priority. -1 specifies no logging at all.
	 * 
	 * @param aPriority The severity of a log priority, the smaller, the less
	 *        severe, the higher, the more severe. As the first "printable"
	 *        priority starts with 0, thge priority can be used as an arrayx
	 *        index.
	 */
	private LogPriority( int aPriority ) {
		_priority = aPriority;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the severity of the log priority.
	 * 
	 * @return aSeverity The severity of a log priority, the smaller, the less
	 *         severe, the higher, the more severe.
	 */
	public int getPriority() {
		return _priority;
	}

	/**
	 * The Log-Level is the String representation of the {@link LogPriority}, as
	 * required by the {@link RuntimeLoggerImpl#setLogLevel(String)}.
	 *
	 * @return The String representation of the {@link LogPriority}.
	 */
	public String getLogLevel() {
		return name();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the priority from the given name, ignoring the case.
	 * 
	 * @param aPriorityName The for which to retrieve the priority.
	 * 
	 * @return The according {@link LogPriority} or null if none was
	 *         determinable.
	 */
	public static LogPriority toProprtiry( String aPriorityName ) {
		for ( LogPriority ePriority : values() ) {
			if ( ePriority.name().equalsIgnoreCase( aPriorityName ) ) {
				return ePriority;
			}
		}
		return null;
	}

	/**
	 * Translates (as good as it gets) a JUL {@link Level} to a
	 * {@link LogPriority}.
	 * 
	 * @param aLevel The JUL {@link Level} to be translated.
	 * 
	 * @return The according {@link LogPriority}.
	 */
	public static LogPriority fromLevel( Level aLevel ) {
		if ( Level.OFF.equals( aLevel ) ) {
			return LogPriority.NONE; // Only JUL logger config level
		}
		if ( Level.SEVERE.equals( aLevel ) ) {
			return LogPriority.ERROR;
		}
		if ( Level.WARNING.equals( aLevel ) ) {
			return LogPriority.WARN;
		}
		if ( Level.CONFIG.equals( aLevel ) ) {
			return LogPriority.NOTICE;
		}
		if ( Level.INFO.equals( aLevel ) ) {
			return LogPriority.INFO;
		}
		if ( Level.FINE.equals( aLevel ) ) {
			return LogPriority.DEBUG;
		}
		if ( Level.FINER.equals( aLevel ) ) {
			return LogPriority.DEBUG;
		}
		if ( Level.FINEST.equals( aLevel ) ) {
			return LogPriority.TRACE;
		}
		return Level.ALL.equals( aLevel ) ? LogPriority.TRACE : INFO;
	}

	/**
	 * Translates (as good as it gets) a {@link LogPriority} to a JUL
	 * {@link Level}.
	 * 
	 * @return The according {@link Level}.
	 */
	public Level toLevel() {
		// @formatter:off
		switch ( this ) {
		case PANIC -> {
			return Level.SEVERE;
		}
		case ALERT -> {
			return Level.SEVERE;
		}
		case CRITICAL -> {
			return Level.SEVERE;
		}
		case ERROR -> {
			return Level.SEVERE;
		}
		case WARN -> {
			return Level.WARNING;
		}
		case NOTICE -> {
			return Level.CONFIG;
		}
		case INFO -> {
			return Level.INFO;
		}
		case DEBUG -> {
			return Level.FINE;
		}
		case TRACE -> {
			return Level.FINEST;
		}
		case DISCARD -> {
			return Level.OFF;
		}
		case NONE -> {
			return Level.OFF;
		}
		}
		// @formatter:on
		return Level.INFO;
	}
}
