// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

import org.refcodes.criteria.BadCriteriaException;
import org.refcodes.criteria.Criteria;
import org.refcodes.tabular.Record;

/**
 * The {@link org.refcodes.logger.TrimLoggerComposite} extends the
 * {@link org.refcodes.logger.QueryLoggerComposite} with the trim logger
 * functionality. In contrast to the
 * {@link org.refcodes.logger.TrimLoggerComposite#log(org.refcodes.tabular.Record)}
 * method, which is forwarded to exactly one of the encapsulated trim logger
 * instances, the
 * {@link org.refcodes.logger.TrimLoggerComposite#deleteLogs(org.refcodes.criteria.Criteria)}
 * and {@link org.refcodes.logger.TrimLoggerComposite#clear()} method calls are
 * forwarded to all of the encapsulated {@link org.refcodes.logger.TrimLogger}
 * instances (in case, partitioning is needed, take a look at the
 * {@link org.refcodes.logger.PartedTrimLogger}).
 *
 * @param <L> The {@link Logger} type to be supported. As the
 *        {@link AbstractLoggerComposite} is being extended to support other
 *        {@link Logger} types, we have to provide this generic parameter (for
 *        example, sub-classes make use of the {@link #getLoggers()} method to
 *        access the encapsulated {@link Logger} instances being sub-types of
 *        the {@link Logger} interface).
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 * 
 * @see TrimLoggerComposite
 */
abstract class AbstractTrimLoggerComposite<L extends TrimLogger<T>, T> extends AbstractQueryLoggerComposite<L, T> implements TrimLogger<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractTrimLoggerComposite} from the provided
	 * {@link Logger} instances.
	 * 
	 * @param aLoggers The {@link Logger} instances to be used for the
	 *        {@link AbstractTrimLoggerComposite}.
	 */
	@SafeVarargs
	public AbstractTrimLoggerComposite( L... aLoggers ) {
		super( aLoggers );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		LoggerUtility.clearLogs( getLoggers() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteLogs( Criteria aCriteria ) throws BadCriteriaException {
		LoggerUtility.deleteLogs( aCriteria, getLoggers() );
	}
}
