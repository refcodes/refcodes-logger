// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger;

/**
 * Provides an accessor for a {@link RuntimeLogger} property.
 */
public interface RuntimeLoggerAccessor {

	/**
	 * Retrieves the logger from the logger property.
	 * 
	 * @return The logger stored by the logger property.
	 */
	RuntimeLogger getRuntimeLogger();

	/**
	 * Provides a mutator for a {@link RuntimeLogger} property.
	 */
	public interface RuntimeLoggerMutator {

		/**
		 * Sets the logger for the logger property.
		 * 
		 * @param aRuntimeLogger The logger to be stored by the logger property.
		 */
		void setRuntimeLogger( RuntimeLogger aRuntimeLogger );
	}

	/**
	 * Provides a {@link RuntimeLogger} property.
	 */
	public interface RuntimeLoggerProperty extends RuntimeLoggerAccessor, RuntimeLoggerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link RuntimeLogger}
		 * (setter) as of {@link #setRuntimeLogger(RuntimeLogger)} and returns
		 * the very same value (getter).
		 * 
		 * @param aRuntimeLogger The {@link RuntimeLogger} to set (via
		 *        {@link #setRuntimeLogger(RuntimeLogger)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RuntimeLogger letRuntimeLogger( RuntimeLogger aRuntimeLogger ) {
			setRuntimeLogger( aRuntimeLogger );
			return aRuntimeLogger;
		}
	}
}
