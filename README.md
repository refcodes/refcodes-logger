# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides flexible logging of any data to any data sink (including files, databases or the console). It supports straight forward, composite (clustering) or partitioning functionality provided by different implementations of the [`Logger`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/latest/org.refcodes.logger/org/refcodes/logger/Logger.html) type. The  [`RuntimeLogger`](https://static.javadoc.io/org.refcodes/refcodes-logger/latest/org.refcodes.logger/org/refcodes/logger/RuntimeLogger.html) type harnesses the [`Logger`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/latest/org.refcodes.logger/org/refcodes/logger/Logger.html) type for logging your runtime log messages and integrates with [`SLF4J`](http://www.slf4j.org) seamlessly (and may also act as an alternative data sink to log to when using [`SLF4J`](http://www.slf4j.org).***

## Getting started ##

> Please refer to the [refcodes-logger: Fancy runtime-logs and highly scalable cloud logging](https://www.metacodes.pro/refcodes/refcodes-logger) documentation for an up-to-date and detailed description on the usage of this artifact.  

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:


```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-logger</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-logger). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-logger).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-logger/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.